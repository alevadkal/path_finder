/// Copyright © 2017 Alexander Kalyuzhnyy. All rights reserved.

#ifndef LOGGER_INTERNAL_H_INCLUDED
#define LOGGER_INTERNAL_H_INCLUDED
#include <string>

class __logger_function
{
public:
    void put_message(const char* message);
    ~__logger_function();
private:
    std::string m_String;
    bool m_NeedLog = false;
};

void __logger_put_string(const char* s);

#endif // LOGGER_INTERNAL_H_INCLUDED
