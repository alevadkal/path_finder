/// Copyright © 2017 Alexander Kalyuzhnyy. All rights reserved.

//#ifndef LOGGER_HPP_INCLUDED
//#define LOGGER_HPP_INCLUDED

#include "logger_internal.h"

#include <string>
#include <iostream>
#include <cstring>
#include <sstream>

#define FATAL 0
#define ERROR 1
#define WARNING 2
#define NOTICE 3
#define INFO 4
#define DEBUG 5
#define VERBOSE 6
#define VERBOSE2 7

#define LOGGER_MAX_MESSAGE_SIZE 512

#ifndef LOG_LEVEL
#define LOG_LEVEL DEBUG
#endif

#define __LOGGER_PUT_MESSAGE(LEVEL, LOG_LVL_STR , ...) \
do \
{ \
    if(LEVEL <= LOG_LEVEL) \
    { \
        std::ostringstream stream; \
        stream << LOG_LVL_STR << "|" << strrchr("/" __FILE__ ,'/')+1 << ":" << __LINE__ << "|" << __FUNCTION__ << "|" << __VA_ARGS__; \
        __logger_put_string(stream.str().c_str()); \
    } \
} \
while(0)

#if LOG_LEVEL > VERBOSE
#define log_verbose2(...) __LOGGER_PUT_MESSAGE(VERBOSE2, "VERBOS2", __VA_ARGS__)
#else
#define log_verbose2(...)
#endif

#if LOG_LEVEL > DEBUG
#define log_verbose(...) __LOGGER_PUT_MESSAGE(VERBOSE, "VERBOSE", __VA_ARGS__)
#else
#define log_verbose(...)
#endif

#define log_debug(...)   __LOGGER_PUT_MESSAGE(DEBUG,   "DEBUG  ", __VA_ARGS__)
#define log_info(...)    __LOGGER_PUT_MESSAGE(INFO,    "INFO   ", __VA_ARGS__)
#define log_notice(...)  __LOGGER_PUT_MESSAGE(NOTICE,  "NOTICE ", __VA_ARGS__)
#define log_warning(...) __LOGGER_PUT_MESSAGE(WARNING, "WARNING", __VA_ARGS__)
#define log_error(...)   __LOGGER_PUT_MESSAGE(ERROR,   "ERROR  ", __VA_ARGS__)
#define log_fatal(...)   __LOGGER_PUT_MESSAGE(FATAL,   "FATAL  ", __VA_ARGS__)

#define LOG_THIS std::hex << (size_t)(this) << std::dec << "|"

#define __LOG_FUN(LEVEL, LOG_LEVEL_STR,...) \
__logger_function __l; \
do \
{ \
    if(LEVEL <= LOG_LEVEL) \
    { \
        std::ostringstream stream; \
        stream << LOG_LEVEL_STR << "|" << strrchr("/" __FILE__ ,'/')+1 << ":" << __LINE__ << "|" << __FUNCTION__ << "|" << __VA_ARGS__; \
        __l.put_message(stream.str().c_str()); \
    } \
} \
while(0)

#define log_fun_verbose(...) __LOG_FUN(VERBOSE, "VERBOSE", __VA_ARGS__)
#define log_fun_debug(...)   __LOG_FUN(DEBUG,   "DEBUG  ", __VA_ARGS__)
#define log_fun_info(...)    __LOG_FUN(INFO,    "INFO   ", __VA_ARGS__)
#define log_fun_notice(...)  __LOG_FUN(NOTICE,  "NOTICE ", __VA_ARGS__)
#define log_fun_warning(...) __LOG_FUN(WARNING, "WARNING", __VA_ARGS__)
#define log_fun_error(...)   __LOG_FUN(ERROR,   "ERROR  ", __VA_ARGS__)
#define log_fun_fatal(...)   __LOG_FUN(FATAL,   "FATAL  ", __VA_ARGS__)
#define log_fun(...)        log_fun_verbose(__VA_ARGS__)

//#endif // LOGGER_H_INCLUDED
