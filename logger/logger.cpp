/// Copyright © 2017 Alexander Kalyuzhnyy. All rights reserved.

#include <mutex>
#include <chrono>
#include <sstream>
#include <iostream>
#include <thread>
#include "logger.hpp"
#include "fstream"

static void print_function(const char* s)
{
    static std::mutex lock;
    //static std::ofstream stream{"log.log",std::ofstream::out};
    std::lock_guard<std::mutex> guard(lock);
    //stream<<s<<std::endl;
    std::cout<<s<<std::endl;

}

void __logger_put_string(const char* s)
{
    static std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
    std::stringstream stream;
    unsigned long long microsec = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count();

    stream << microsec/1000000 << ".";
    std::streamsize old= stream.width(6);
    char old_fill = stream.fill('0');
    stream << microsec % 1000000;
    stream.width(old);
    stream.fill(old_fill);
    stream << "|" << std::hex<<std::this_thread::get_id()<< std::dec<<"|" << s;
    print_function(stream.str().c_str());
}

void __logger_function::put_message(const char* s)
{
    {
        std::stringstream stream;
        stream << s << "[enter]";
        __logger_put_string(stream.str().c_str());
    }
    std::stringstream stream;
    stream << s << "[exit]";
    m_String = stream.str();
    m_NeedLog = true;
}

__logger_function::~__logger_function()
{
    if(!m_NeedLog) return;
    __logger_put_string(m_String.c_str());
}
