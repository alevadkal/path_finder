/// Copyright © 2017 Alexander Kalyuzhnyy. All rights reserved.

#ifndef VECTOR2_HPP
#define VECTOR2_HPP
#include "Vector.hpp"

typedef tVector<double, 2> tVector2;

struct tSystemState
{
    tVector2 y;
    tVector2 dydt;
    operator double*()
    {
        return (double*)this;
    }
};

inline std::ostream& operator <<(std::ostream& s, const tSystemState& data)
{
    return s<<"{\"y\":"<< data.y << ",\"dydt\":"<< data.dydt << "}";
}

/*
template<typename T, unsigned size>
std::ostream& operator <<(std::ostream& s, const tVector2& data)
{
    s<<"[";
    bool first = true;
    for(auto dat:data.m_Data)
    {
        if(!first)
        {
            s<<",";
        }
        else
        {
          first = false;
        }
        s<<dat;
    }
    s<<"]";
    return s;
}
*/

#endif // VECTOR2_HPP
