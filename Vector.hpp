/// Copyright © 2017 Alexander Kalyuzhnyy. All rights reserved.

#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <ostream>
#include <initializer_list>
#include <exception>
#include <cmath>

template<typename T, unsigned size>
struct tVector
{
    T m_Data[size];

public:

    tVector() = default;
    ~tVector() = default;

    tVector(const std::initializer_list<T>& list)
    {
        if(list.size() != size) throw std::exception();

        auto initVal = list.begin();
        for(T& val:m_Data) val = *initVal++;
    }

    T& operator[](unsigned i)
    {
        return m_Data[i];
    }

    const T& operator[](unsigned i) const
    {
        return m_Data[i];
    }

    unsigned dimension() const
    {
        return size;
    }

    tVector& operator +=(const tVector& obj)
    {
        for(unsigned i=0; i<size; i++)
        {
            m_Data[i] += obj.m_Data[i];
        }
        return *this;
    }

    tVector& operator -=(const tVector& obj)
    {
        for(unsigned i=0; i<size; i++)
        {
            m_Data[i] -= obj.m_Data[i];
        }
        return *this;
    }

    tVector& operator *=(const T& val)
    {
        for(unsigned i=0; i<size; i++)
        {
            m_Data[i] *= val;
        }
        return *this;
    }
    tVector& operator *=(const tVector& val)
    {
        for(unsigned i=0; i<size; i++)
        {
            m_Data[i] *= val[i];
        }
        return *this;
    }

    tVector& operator /=(const T& val)
    {
        for(unsigned i=0; i<size; i++)
        {
            m_Data[i] /= val;
        }
        return *this;
    }

    tVector operator +(const tVector& obj) const
    {
        tVector result;
        for(unsigned i=0; i<size; i++)
        {
            result.m_Data[i] = m_Data[i] + obj.m_Data[i];
        }
        return result;
    }

    tVector operator -(const tVector& obj) const
    {
        tVector result;
        for(unsigned i=0; i<size; i++)
        {
            result.m_Data[i] = m_Data[i] - obj.m_Data[i];
        }
        return result;
    }

    tVector operator *(const T& val) const
    {
        tVector result;
        for(unsigned i=0; i<size; i++)
        {
            result.m_Data[i] = m_Data[i] * val;
        }
        return result;
    }

    tVector operator *(const tVector& val) const
    {
        tVector result;
        for(unsigned i=0; i<size; i++)
        {
            result.m_Data[i] = m_Data[i] * val[i];
        }
        return result;
    }

    tVector operator /(const T& val) const
    {
        tVector result;
        for(unsigned i=0; i<size; i++)
        {
            result.m_Data[i] = m_Data[i] / val;
        }
        return result;
    }

    tVector operator -() const
    {
        tVector result;
        for(unsigned i=0; i<size; i++)
        {
            result.m_Data[i] = -m_Data[i];
        }
        return result;
    }

    tVector direction()
    {
        return *this/length();
    }

    T lengthSq() const
    {
        T sqSum{};
        for(T val: m_Data)
        {
            sqSum += val * val;
        }
        return sqSum;
    }

    T length() const
    {

        return sqrt(lengthSq());
    }

    void clean()
    {
        for(unsigned i=0; i<size; i++)
        {
            m_Data[i] = 0;
        }
    }
    operator T*()
    {
        return m_Data;
    }
};

/*template<typename T, unsigned size>
T distance(const tVector<T, size>& first, const tVector<T, size>& second)
{
    return (first - second).length();
}*/

template<typename T, unsigned size>
tVector<T, size> projection(const tVector<T, size>& first, const tVector<T, size>& second)
{
    return (first*second)/second.length();
}


template<typename T, unsigned size>
std::ostream& operator <<(std::ostream& s, const tVector<T, size>& data)
{
    s<<"[";
    bool first = true;
    for(auto dat:data.m_Data)
    {
        if(!first)
        {
            s<<",";
        }
        else
        {
          first = false;
        }
        s<<dat;
    }
    s<<"]";
    return s;
}

#endif // VECTOR_HPP_INCLUDED

