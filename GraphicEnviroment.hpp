/// Copyright © 2017 Alexander Kalyuzhnyy. All rights reserved.

#ifndef GRAPHICENVIROMENT_HPP
#define GRAPHICENVIROMENT_HPP
#include <SDL2/SDL.h>

class tGraphicEnviroment
{
public:
    static tGraphicEnviroment& instance();
    SDL_Window* getWindow();
    SDL_GLContext getGlContext();
    bool init();
    ~tGraphicEnviroment();
private:
    bool m_IsInit{false};
    static constexpr const char * PROGRAM_NAME = "graw wars";
    bool setOpenGLAttributes();
    void cleanup();
    tGraphicEnviroment();
    tGraphicEnviroment(const tGraphicEnviroment& obj) = delete;
    tGraphicEnviroment operator=(const tGraphicEnviroment& obj) = delete;

    SDL_Window* m_MainWindow;
    SDL_GLContext m_MainContext;
};

#endif // GRAPHICENVIROMENT_HPP
