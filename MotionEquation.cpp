#include <gsl/gsl_odeiv.h>
#include <gsl/gsl_errno.h>
#include "MotionEquation.hpp"
#include "common.hpp"
#include "logger.hpp"

const gsl_odeiv2_system* tMotionEquation::getGslSystem()
{
    log_fun(LOG_THIS);
    if(m_System == nullptr)
    {
        if(isJacobianNeed())
        {
            m_System = new(std::nothrow) gsl_odeiv2_system{systemFunction, systemJacobian, getDimension(), this};
        }
        else
        {
            m_System = new(std::nothrow) gsl_odeiv2_system{systemFunction, nullptr, getDimension(), this};
        }
    }
    return m_System;
}

int tMotionEquation::jacobian(double t, const double y[], double * dfdy, double dfdt[])
{
    UNUSED(t);
    UNUSED(y);
    UNUSED(dfdy);
    UNUSED(dfdt);
    log_fun("");
    return GSL_SUCCESS;
}

bool tMotionEquation::isJacobianNeed()
{
    log_fun(LOG_THIS);
    return false;
}

int  tMotionEquation::systemFunction (double t, const double y[], double dydt[], void * params)
{
    log_fun("");
    return ((tMotionEquation*)params)->function(t, y, dydt);
}

int tMotionEquation::systemJacobian(double t, const double y[], double dfdy[], double dfdt[], void * params)
{
    log_fun("");
    return ((tMotionEquation*)params)->jacobian(t, y, dfdy, dfdt);
}

