/// Copyright © 2017 Alexander Kalyuzhnyy. All rights reserved.

#include <iostream>
#include <vector>
#include <cmath>
#include <unistd.h>
#include <cmath>
#include <GL/gl.h>
#include "BasicHandicap.hpp"
#include "NormalAccelerationEquation.hpp"
#include "Rkf45Integrator.hpp"
#include "GraphicEnviroment.hpp"
#include "common.hpp"
#include "logger.hpp"

struct
{
    struct
    {
        struct
        {
            double epsAbs;
            double epsRel;
            double hStart;
        } accuracy;
        double tStart;
        double tStep;
        unsigned steps;
    } integrator;
    struct
    {
        tVector2 position;
        tVector2 speed;
        tVector2 acceleration;
    } handicap;
    struct
    {
        tVector2 startPosition;
        double speedAbs;
        unsigned angularResolution;
        double limiter;
        double scale;
        double propagationSpeed;

    } equation;

    struct
    {
        double xMax;
        double yMax;
        double xMin;
        double yMin;
    } screenBorder;

} params;


void init()
{
    params.integrator.accuracy.epsRel = 1.E-6;
    params.integrator.accuracy.epsAbs = 0.0;
    params.integrator.accuracy.hStart = 1.0E-6;
    params.integrator.tStart = 0.0;
    params.integrator.tStep = 0.5;
    params.integrator.steps = 1000;
    params.handicap.position = {-100, 100};
    params.handicap.speed = {10*sqrt(2)/2, -10*sqrt(2)/2};
    params.handicap.acceleration = {-0.1,0.2};
    params.equation.startPosition = {0,0};
    params.equation.speedAbs = 10;
    params.equation.limiter = 5;
    params.equation.scale = 1;
    params.equation.propagationSpeed = 2000;
    params.equation.angularResolution = 128;
    params.screenBorder.xMax = 200;
    params.screenBorder.yMax = 200;
    params.screenBorder.xMin = -200;
    params.screenBorder.yMin = -200;
}

bool checkBorder(const tVector2& vec)
{
    if(vec[0] > params.screenBorder.xMax) return false;
    if(vec[1] > params.screenBorder.yMax) return false;
    if(vec[0] < params.screenBorder.xMin) return false;
    if(vec[1] < params.screenBorder.xMin) return false;
    return true;
}

void drawPath(const std::vector<tVector2>& vector)
{
    log_fun_debug("draw traectory");
    glColor4f(1, 1, 1, 1);
    glBegin(GL_LINE_STRIP);
    for(auto vertex:vector) glVertex2d(vertex[0], vertex[1]);
    glEnd();
}

void drawHandicapData(const tBasicHandicap& handicap, int figure, float alpha = 1)
{
    double tStart = params.integrator.tStart;
    double tStep = params.integrator.tStep;
    glColor4f(1.0,0.0,0.0,alpha);
    glBegin(figure);

    for(unsigned  i = 0; i<params.integrator.steps; i++)
    {
        double tNext = tStart + i * tStep;
        tVector2 position = handicap.getPosition(tNext);
        glVertex2d(position[0], position[1]);
    }
    glEnd();
}

void drawEquationData(tMotionEquation& equation, tVector2 startSpeed,  int figure, float alpha = 1)
{
    double tStart = params.integrator.tStart;
    double tStep = params.integrator.tStep;
    tVector2 yStart = params.equation.startPosition;
    double t = tStart;
    tRkf45Integrator integrator(
        equation.getGslSystem(),
        params.integrator.accuracy.epsAbs,
        params.integrator.accuracy.epsRel,
        params.integrator.accuracy.hStart
    );
    tSystemState systemState = {yStart,startSpeed};
    glBegin(figure);
    glColor4f(0.0,1,0.0,alpha);
    for(unsigned i = 0; i<params.integrator.steps; i++)
    {
        double tNext = tStart + i * tStep;
        glVertex2d(systemState.y[0],systemState.y[1]);
        integrator.run(systemState, &t, tNext);
        checkBorder(systemState.y);
    }
    glEnd();
}
int main()
{
    log_fun("");
    init();
    tBasicHandicap handicap
    {
        params.handicap.position,
        params.handicap.speed,
        params.handicap.acceleration
    };
    tBasicEquation equation(
        handicap,
        params.equation.limiter,
        params.equation.scale,
        params.equation.propagationSpeed
    );
    tVector2 yStart = params.equation.startPosition;


    if(tGraphicEnviroment::instance().init() == false)
    {
        log_error("Can't init graphic enviroment");
    }

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    drawHandicapData(handicap, GL_POINTS);

    unsigned circularGrads = params.equation.angularResolution;

    double gradStep = (2 * M_PI) / circularGrads;

    for(double j = 0; j < circularGrads ; j++)
    {

        tVector2 startSpeed = {cos(gradStep*j)*params.equation.speedAbs, sin(gradStep*j)*params.equation.speedAbs};
        drawEquationData(equation, startSpeed, GL_POINTS);
        drawEquationData(equation, startSpeed, GL_LINE_STRIP, 0.1);


    }
    glFlush();
    SDL_GL_SwapWindow(tGraphicEnviroment::instance().getWindow());

    while(1)
    {
        sleep(1);

    }
    return 0;
}
