/// Copyright © 2017 Alexander Kalyuzhnyy. All rights reserved.

#ifndef COMMON_HPP
#define COMMON_HPP

#define UNUSED(VALUE) (void)VALUE;

#endif // COMMON_HPP
