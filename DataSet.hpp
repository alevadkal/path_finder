/// Copyright © 2017 Alexander Kalyuzhnyy. All rights reserved.

#ifndef OURSYSTEM_HPP
#define OURSYSTEM_HPP
#include "MotionEquation.hpp"

//TODO Это черновик для динамического составления систем дифференциальных уравнений большой размерности. ну вот например захочется нам посмотреть как два объекта взаимодействуют друг с другом.
typedef (tFunction*)(double t, const double y[], double dydt[], void * params);

struct tPoint
{
    double x;
    double y;
    tPoint() = default;

};

///
/// \brief The tDataSet struct
///
struct tDataSet
{
    typedef (tFunction*)(double t, const double y[], double dydt[], void * params);

    tPoint* y = nullptr;
    tPoint* dydt = nullptr;
    tFunction* function = nullptr;
    void** params = nullptr;

    struct tEquation
    {
        tPoint y;
        tPoint dydt;
        tFunction function;
        void* params;
    };

    tDataSet() = default;
    void push(const tEquation& equation)
    {

    }


    void getSystem();


    static systemFunction()
};


#endif // OURSYSTEM_HPP
