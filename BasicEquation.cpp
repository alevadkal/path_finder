/// Copyright © 2017 Alexander Kalyuzhnyy. All rights reserved.

#include "BasicEquation.hpp"
#include <gsl/gsl_errno.h>
#include "logger.hpp"

tBasicEquation::tBasicEquation(tBasicHandicap handicap, double limiter, double scale, double propagationSpeed):
    m_Handicap{handicap},
    m_Limiter{limiter * limiter},
    m_Scale{scale},
    m_PropagationSpeed{propagationSpeed}
{
    log_info(LOG_THIS << *this);
}

int tBasicEquation::function(double t, const double* y, double* dydt)
{
    log_fun(LOG_THIS);
    tVector2 handicapPosition = m_Handicap.getPosition(t);
    const tSystemState& input = *((const tSystemState*)y);
    tSystemState& output = *((tSystemState*)dydt);

    tVector2 sub =  handicapPosition - input.y;
    tVector2 direction = sub/sub.length();
    output.y =  input.dydt;
    output.dydt =  direction*(m_PropagationSpeed * pow((sub.lengthSq() + m_Limiter),-m_Scale));
    return GSL_SUCCESS;
}

unsigned tBasicEquation::getDimension()
{
    return sizeof(tSystemState)/sizeof(double);
}

std::ostream& operator <<(std::ostream& s, const tBasicEquation& data)
{
    return s<<"{"
    <<"\"handicap\":"<< data.m_Handicap << ","
    <<"\"limiter\":"<< data.m_Limiter << ","
    <<"\"scale\":"<< data.m_Scale << ","
    <<"\"propagation_speed\":"<< data.m_PropagationSpeed
    <<"}";

}
