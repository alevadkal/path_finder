/// Copyright © 2017 Alexander Kalyuzhnyy. All rights reserved.

#ifndef INTEGRATOR_H_INCLUDED
#define INTEGRATOR_H_INCLUDED
#include <exception>
#include <stdexcept>
#include <functional>
#include <gsl/gsl_odeiv2.h>

class tRkf45Integrator
{
public:
    ///
    /// \brief tRkf45Integrator
    /// \param system дифференциальное уравниение для решени
    /// \param epsAbs абсолютная ошибка измерения
    /// \param epsRel относительная ошибка измерения
    ///
    tRkf45Integrator(const gsl_odeiv2_system* system, const double epsAbs = 0.0, const double epsRel = 1.E-6, const double hStart = 1.E-6);
    ~tRkf45Integrator();
    ///
    /// \brief run запускает процесс интегрирования
    /// \param yStart стартовое знанчение функции - изменяется в процессе ингегрирования
    /// \param tStart начальное время интегрирования
    /// \param tEnd конечное время интегрирования
    /// \param tStartStep начальный шаг интегрирования
    /// \return GSL_SUCCESS при удаче или какую нибудь ошибку библиотеки GSL
    ///
    int run(double* yStart, double* tStart, double tEnd);

    ///
    /// \brief reset используйте если хотет провести два независимых интегрирования с одним экземпляром класса
    /// \return GSL_SUCCESS при удаче или какую нибудь ошибку библиотеки GSL
    ///
    int reset();

    int resetHStart(const double hStart);
protected:
    gsl_odeiv2_driver *m_Odeiv2Driver = nullptr;
private:
    void freeData();
};
#endif // INTEGRATOR_H_INCLUDED
