/// Copyright © 2017 Alexander Kalyuzhnyy. All rights reserved.

#ifndef BASICHANDICAP_HPP
#define BASICHANDICAP_HPP
#include "Vector2.hpp"
///
/// \brief The tBasicHandicap struct элементарное уравнение движения для метериальной точки.
///
struct tBasicHandicap
{
    tVector2 m_StartPosition;
    tVector2 m_StartSpeed;
    tVector2 m_Acceleration;

    tBasicHandicap(tVector2 position = {0,0}, tVector2 speed = {0,0}, tVector2 acceleration = {0,0}):
        m_StartPosition(position),
        m_StartSpeed(speed),
        m_Acceleration(acceleration){}

    ///
    /// \brief getPosition
    /// \param t время
    /// \return возвращает положение точки для текущего времсени
    ///
    tVector2 getPosition(double t) const
    {
        return m_StartPosition+ m_StartSpeed*t + m_Acceleration*((t*t)/2);
    }
};

inline std::ostream& operator <<(std::ostream& s, const tBasicHandicap& data)
{
    return s<<"{"
    <<"\"position\":"<< data.m_StartPosition << ","
    <<"\"speed\":"<< data.m_StartSpeed << ","
    <<"\"acceleration\":"<< data.m_Acceleration
    <<"}";
}

#endif // BASICHANDICAP_HPP
