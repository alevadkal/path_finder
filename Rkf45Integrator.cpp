/// Copyright © 2017 Alexander Kalyuzhnyy. All rights reserved.

#include <gsl/gsl_errno.h>
#include "Rkf45Integrator.hpp"
#include "Vector2.hpp"
#include "logger.hpp"

tRkf45Integrator::tRkf45Integrator(const gsl_odeiv2_system* system, const double epsAbs, const double epsRel, const double hStart):
    m_Odeiv2Driver{gsl_odeiv2_driver_alloc_y_new (system, gsl_odeiv2_step_rk8pd, hStart, epsAbs, epsRel)}
{
    if(m_Odeiv2Driver == nullptr)
    {
        throw std::exception();
    }
}

tRkf45Integrator::~tRkf45Integrator()
{
    log_fun(LOG_THIS);
    if(m_Odeiv2Driver) gsl_odeiv2_driver_free(m_Odeiv2Driver);
}

int tRkf45Integrator::run(double* yStart, double* tStart, double tEnd)
{
    return gsl_odeiv2_driver_apply (m_Odeiv2Driver, tStart, tEnd, yStart);
}

int tRkf45Integrator::reset()
{
    return gsl_odeiv2_driver_reset(m_Odeiv2Driver);
}

int tRkf45Integrator::resetHStart(const double hStart)
{
    return gsl_odeiv2_driver_reset_hstart(m_Odeiv2Driver, hStart);
}

