/// Copyright © 2017 Alexander Kalyuzhnyy. All rights reserved.

#ifndef BASICEQUATION_HPP
#define BASICEQUATION_HPP
#include "MotionEquation.hpp"
#include "BasicHandicap.hpp"

struct tBasicEquation: public tMotionEquation
{
    tBasicHandicap m_Handicap;
    double m_Limiter;
    double m_Scale;
    double m_PropagationSpeed;

    tBasicEquation(tBasicHandicap handicap = {{0,0},{1,1}, {1,-1}}, double limiter = 1, double scale = 1, double propagationSpeed = 100);
protected:
    virtual int function(double t, const double* input, double* output) override;
private:
    virtual unsigned getDimension() override;
};

inline std::ostream& operator <<(std::ostream& s, const tBasicEquation& data);

#endif // BASICEQUATION_HPP
