/// Copyright © 2017 Alexander Kalyuzhnyy. All rights reserved.

#ifndef EQUATION_HPP_INCLUDED
#define EQUATION_HPP_INCLUDED
#include <gsl/gsl_odeiv2.h>
#include "common.hpp"

class tMotionEquation
{
public:
    tMotionEquation() = default;
    virtual ~tMotionEquation() = default;
    ///
    /// \brief getGslSystem
    /// \return gslSystem возвращает структуру, которая используется решателями GSL
    /// если isJacobianNeed возвращает ложь то в качестве якобиана передаётся нулевой указатель.
    /// в противном случае передается указатель на функцию якобиана.
    ///
    const gsl_odeiv2_system* getGslSystem();
protected:
    ///
    /// \brief function реализация дифференциального уравнения
    /// \param t время
    /// \param y входящее значение
    /// \param dydt исходящее значение
    /// \return для успешного завершения всегда должен возвращать GSL_SUCCESS
    ///
    virtual int function(double t, const double y[], double dydt[]) = 0;
    ///
    /// \brief jacobian функция якобиана которая используеется некоторыми решателями.
    /// \param t
    /// \param y
    /// \param dfdy
    /// \param dfdt
    /// \return для стандартной реализации всегда возвращет GSL_FAILURE
    ///
    virtual int jacobian(double t, const double y[], double * dfdy, double dfdt[]);

    gsl_odeiv2_system* m_System = nullptr;
private:
    ///
    /// \brief getDimension используется для генерации системы для решателя.
    /// \return возвращет реазмерность системы.
    ///
    virtual unsigned getDimension() =0;
    ///
    /// \brief isJacobianNeed
    /// \return для стандартной реализации всегда возвращает ложь
    ///
    virtual bool isJacobianNeed();
    ///
    /// \brief systemFunction используется как указатель на функцию для системы GSL
    /// \param t
    /// \param y
    /// \param dydt
    /// \param params
    /// \return
    ///
    static int  systemFunction (double t, const double y[], double dydt[], void * params);
    ///
    /// \brief systemJacobian используется как указатель на якобиан для системы GSL
    /// \param t
    /// \param y
    /// \param dfdy
    /// \param dfdt
    /// \param params
    /// \return
    ///
    static int systemJacobian(double t, const double y[], double * dfdy, double dfdt[], void * params);
};

#endif // EQUATION_HPP_INCLUDED
