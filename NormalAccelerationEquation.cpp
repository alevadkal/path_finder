/// Copyright © 2017 Alexander Kalyuzhnyy. All rights reserved.

#include <gsl/gsl_errno.h>
#include "NormalAccelerationEquation.hpp"
#include "logger.hpp"

int tNormalAccelerationEquation::function(double t, const double* y, double* dydt)
{
    int ret = tBasicEquation::function(t,y,dydt);
    const tSystemState& input = *((const tSystemState*)y);
    tSystemState& output = *((tSystemState*)dydt);
    output.dydt = output.dydt - projection(output.dydt, input.dydt);
    return ret;
}
