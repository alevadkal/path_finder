#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "headers.h"

double *Yp,*F1,*F2,*F3,*F4,*F5,h,savre,savae;
int nfe,kop,init,jflag,kflag;
double eps,u26;

double fmax(double x, double y)
 {
  return (x>y)?x:y;
 }

void fehl(void (*f)(double,double*,double*, eq_par &eqpar),
          int neqn,
          double *y,
          double t,
          double h,
          double *yp,
          double *f1,
          double *f2,
          double *f3,
          double *f4,
          double *f5,
          double *s,
		  eq_par &eqpar
         )
{
 double ch;
 int k;

 ch=h/4;
 for (k=0;k<neqn;k++) f5[k]=y[k]+ch*yp[k];
 (*f)(t+ch,f5,f1,eqpar);
 ch=3.*h/32;
 for (k=0;k<neqn;k++)  f5[k]=y[k]+ch*(yp[k]+3.*f1[k]);
 (*f)(t+3.*h/8.,f5,f2,eqpar);
 ch=h/2197.;
 for (k=0;k<neqn;k++) f5[k]=y[k]+ch*(1932.*yp[k]+(7296.*f2[k]-7200.*f1[k]));
 (*f)(t+12.*h/13.,f5,f3,eqpar);
 ch=h/4104.;
 for (k=0;k<neqn;k++)
   f5[k]=y[k]+ch*((8341.*yp[k]-845.*f3[k])+(29440.*f2[k]-32832.*f1[k]));
 (*f)(t+h,f5,f4,eqpar);
 ch=h/20520.;
 for (k=0;k<neqn;k++)
   f1[k]=y[k]+ch*((-6080.*yp[k]+(9295.*f3[k]-5643.*f4[k]))+
         (41040.*f1[k]-28352.*f2[k]));
 (*f)(t+h/2,f1,f5,eqpar);
 ch=h/7618050.;
 for (k=0;k<neqn;k++)
  s[k]=y[k]+ch*((902880.*yp[k]+(3855735.*f3[k]-1371249.*f4[k]))+
       (3953664.*f2[k]+277020.*f5[k]));
}

void free_rkf(void)
 {
  delete Yp;
  delete F1;
  delete F2;
  delete F3;
  delete F4;
  delete F5;
 }

int rkf45n(void (*f)(double,double*,double*, eq_par& eqpar),
          int neqn,
          double *y,
          double *tin,
          double tout,
          double  *relerr,
          double abserr,
          int *flag,
		  eq_par &eqpar
         )
{
 int mflag,ex_cont,maxnfe=3000,k,output,hfaild;
 double epsp1,rer,remin=1e-13,dt,a,toln,tol,ypk,ae,scale,hmin,eeoet,et,ee,
        s,esttol,h1,t;

 t=*tin;
 mflag=abs(*flag);
 if (neqn<1 || *relerr<0. || abserr<0. || mflag==0 || mflag>8)
    return (*flag=8);
 if (mflag==1)
   {
    eps=1.;
    do { eps/=2.; epsp1=eps+1.;} while (epsp1>1.);
    u26=eps*26.;
    Yp=new double[neqn];
    F1=new double[neqn];
    F2=new double[neqn];
    F3=new double[neqn];
    F4=new double[neqn];
    F5=new double[neqn];
    if (Yp==NULL || F1==NULL || F2==NULL|| F3==NULL|| F4==NULL|| F5==NULL)
       return(*flag=9);
   }
 else
   {
    if (t==tout && kflag!=3) {*tin=t;return (*flag=8);}
    ex_cont=3;
    if (mflag==2)
      {
       if (kflag==3 || init==0) ex_cont=2;
       else if (kflag==4) ex_cont=1;
       else if (kflag==5 && abserr==0.) exit(1);
       else if (kflag==6 && *relerr<=savre && abserr<=savae) exit(1);
      }
    else
      {
       if (*flag==3) ex_cont=2;
       else if (*flag==4) ex_cont=1;
       else if (*flag==5 && abserr>0.) ex_cont=2;
       else exit(1);
      }
    switch (ex_cont)
      {
       case 1:  nfe=0;
                if (mflag==2) break;
       case 2:  *flag=jflag;
                if (kflag==3) mflag=abs(*flag);
      }
   }
 jflag=*flag;
 kflag=0;
 savre=*relerr;
 savae=abserr;
 rer=2.*eps*remin;
 if (*relerr<rer) {*relerr=rer; *tin=t; return (kflag=*flag=3);}
 dt=tout-t;
 if (mflag==1)
   {
    init=kop=0;
    a=t; (*f)(a,y,Yp,eqpar);
    nfe=1;
    if (t==tout) {*tin=t; return (*flag=2);}
   }
 if (init==0)
   {
    init=1;
    h=fabs(dt);
    toln=0;
    for (k=0;k<neqn;k++)
     {
      tol=*relerr*fabs(y[k])+abserr;
      if (tol>0.)
        {
         toln=tol;
         ypk=fabs(Yp[k]);
         if (ypk*pow(h,5.)>tol) h=pow(tol/ypk,.2);
        }
      }
     if (toln<=0.) h=0.;
     h=fmax(h,u26*fmax(fabs(t),fabs(dt)));
     jflag=(*flag>0)?2:-2;
   }
 h=(dt>0.)?fabs(h):-fabs(h);
 if (fabs(h)>=2.*fabs(dt))kop++;
 if (kop>100) {kop=0; *tin=t; return (*flag=7);}
 if (fabs(dt)<u26*fabs(t))
   {
    for (k=0;k<neqn;k++) y[k]+=dt*Yp[k];
    (*f)(a=tout,y,Yp,eqpar);
    nfe++;
    *tin=t=tout;
    return (*flag=2);
   }
 output=0;
 scale=2./(*relerr);
 ae=scale*abserr;
 do
  {
   hfaild=0;
   hmin=u26*fabs(t);
   dt=tout-t;
   if (fabs(dt)<2.*fabs(h))
     if (fabs(dt)<=fabs(h)) { output=1; h=dt;}
     else h=dt/2;
   do
    {
     if (nfe>maxnfe) {*tin=t; return (kflag=*flag=4);}
     fehl(f,neqn,y,t,h,Yp,F1,F2,F3,F4,F5,F1,eqpar);
     nfe+=5;
     eeoet=0.;
     for (k=0;k<neqn;k++)
      {
       et=fabs(y[k])+fabs(F1[k])+ae;
       if (et<=0.){*tin=t; return (*flag=5);}
       ee=fabs((-2090.*Yp[k]+(21970.*F3[k]-15048.*F4[k]))+
               (22528.*F2[k]-27360.*F5[k]));
       eeoet=fmax(eeoet,ee/et);
      }
     esttol=fabs(h)*eeoet*scale/752400.;
     if (esttol>1.)
       {
        hfaild=1;
        output=0;
        s=.1;
        if (esttol<59049.) s=.9/pow(esttol,.2);
        h*=s;
       }
    } while(esttol>1. && fabs(h)>hmin);
   if (fabs(h)<=hmin) {*tin=t; return (kflag=*flag=6);}
   t+=h;
   for (k=0;k<neqn;k++) y[k]=F1[k];
   (*f)(a=t,y,Yp,eqpar);
   nfe++;
   s=5.;
   if (esttol>1.889568e-4) s=.9/pow(esttol,.2);
   if (hfaild) if(s>1.) s=1.;
   h1=fmax(s*fabs(h),hmin);
   h=(h>0)?h1:-h1;
   if (output) {*tin=tout; return (*flag=2);}
  } while (*flag>0);
 *tin=t;
 return(*flag=-2);
}

