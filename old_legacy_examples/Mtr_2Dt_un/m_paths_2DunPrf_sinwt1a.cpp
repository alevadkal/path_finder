/*This program contains the following parameters:
< >. This program calculates the trajectory before second step after overcome 
the boundary (x-x_gr<0 (gra"nze) or t<tout), and then - from last but one step
the zeroin finds null of projection by solving ODE with the help of rkf45.
The II-order system of differential Langevenian equations contains parameters 
(in the second equation): a & epsn(small). The noise covariation matrix 
contains coefficients: Q_11, Q_12, d_21, Q_22.
  The initial conditions are reading from the file "constansts_many.txt",
in the file "renew" are written only three last sets of variables.
Time step in Runge-Kutta routine is AUTOMATIC.
��������� ������� ���������� ������� �� ������� � ������������ x_st+first_xsdvig,y_st �
x_st+scnd_xsdvig,y_st. ��� ����������  �������
(scnd__xsdvig*scnd__xsdvig < first_xsdvig*first_xsdvig) ��������� �������� ������ first_xsdvig,
�������� scnd_xsdvig �������������� ��� ����� ������� ����������� ������������� ����������
� ������ y=y_st. ��� ������������ ����� ������� � first_xsdvig � scnd_xsdvig==scnd__xsdvig 
������� �� ��������� �������. ��������� ���������� ��� ����������� ���������� � ��������,
������ �������� ������������� �����������.  
�������: ������������� ������ ����� ����������� � ������������ (�� �������� ����������� 
����� ���������� �� ����� �����������) � �������� ������������� ���� �������� 
� ���� ������ �����������.
������� ������� � ������������ ����� ��� ������ �������������� �� h-������.
�������� ��� �������� ����� ����� ������ ������� ���������� m_paths_2DunPrf_lin4n4a.cpp.
*/
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream.h>
#include <fstream.h>
#include <iomanip.h>
#include "reading_sinwt.h"
#include "FHN_pr_sinwt.h"
#include "appr.h"
#include "separ.h"
#include "init_cond.h"
#include "current.h"
#include "cov_mtrx_2D.h"
#define nCOLUMNS 2500    
#define nROWS 100
#define MN nCOLUMNS*nROWS
#define QNT 50
#define DQNT 2*QNT

//char ini_file[60]; //?

double y_a, prj, t_out;
//double paths[MN];
int q1,q2;
double pref_mssv0[DQNT];
double pref_mssv_sdvx[DQNT];
double pref_mssv_sdvy[DQNT];
double S_mssv0[DQNT];
double S_mssv_sdvx[DQNT];
double S_mssv_sdvy[DQNT];
double ms_tg_alph[DQNT];
double sepr_sdvg[DSPR];
void free_rkf(void); //?
int init_cond1(double *,int,int,double);
int init_cond2(double *,int);
int rkf45(void(*)(double,double*,double*),int,double*,double*,double,
                  double*,double,int*);
double zeroinnew1(double, double, double(double,double*),double*,double);

FILE *global;

double call0(double xsdvig_ini)
{
double t,tout,vx,vy,sq_v; 
int condition;
double abserr=0.;//, x_xgr=-1.;
//double relerr=1.e-6;
int flag=-1, q;
int ini_flag=-1;

//char buf[60]; // ?
t=0;tout=tout1;
double *x;
x=new double[9];
      i=0;j=0;
init_cond1(x, fl_in, 0,xsdvig_ini);

//int init_cond(double *x,int fl_in)

FILE *real0;
//real=fopen("d:\\polovinkin\\Ml\\real.dat","w");
real0=fopen("real0.dat","w");
/*
if (t<tout && x_xgr<0) condition = 1;
else if (t<tout && x_xgr >= 0 && (x[1] < x[0]-x[0]*x[0]*x[0]/3) && unic) condition = 1;
else condition = 0;

if (unic==1)
}*/
//while(t<tout && x_xgr<0)
//condition=1;

u_bef[1]=y_st;
u_aft[1]=y_st;

while(ini_flag<1)

{
	switch(rkf45(right_part,9,x,&t,tout,&relerr,abserr,&flag))
     {
     case 2:
     break;
     case -2:
vx =  Q_11*x[2] + ax(x[0],x[1],t);
vy =  Q_22*x[3] + ay(x[0],x[1],a, ro,epsn,t);
t_out=t;

	if((u_bef[1]-y_st)*(u_aft[1]-y_st)<0)	ini_flag++;

	u_2bef[0]=u_bef[0];
	u_2bef[1]=u_bef[1];
	u_bef[0]=u_aft[0];
	u_bef[1]=u_aft[1];
	
	u_aft[0]=x[0];
	u_aft[1]=x[1];
	u_aft[2]=x[2];
	u_aft[3]=x[3];
	u_aft[4]=x[4];
	u_aft[5]=x[5];
	u_aft[6]=Q_11*x[2] + ax(x[0],x[1],t);
	u_aft[7]=Q_22*x[3] + ay(x[0],x[1],a,ro,epsn,t);
	u_aft[8]=x_xgr;
	x_xgr=x[0];

//	 if (unic==1)
fprintf(real0,"%lg \t  %lg \t  %lg  \t  %lg  \t  %lg  \t  %lg \n",t,x[0],x[1],x[4],x[5]);
     i++;
     j++;
  
	 if (i==m1)
         i=0;
             break;
     case 5: abserr+=1e-9;
             flag=-2;
             break;
      case 6: relerr*=10;
             flag=-2;
             break;
     case 7: flag=-2;
             break;
     case 8: printf("\n Incorrect call of RKF45");
             exit(1);
     case 9: printf("\n No enough memory for RKF45");
             exit(1);
    }        
  }

free_rkf();
delete x;

fclose(real0);
return lin1_appr(u_2bef[1],u_bef[1],u_2bef[0],u_bef[0],y_st)-x_st;
}

 
double call1(double xsdvig_in, double*mssv)
{
double t,tout,vx,vy,sq_v; 
int condition;
double abserr;  
abserr=0.;//, x_xgr=-1.;
//double relerr=1.e-6;
int flag=-1, q;
//char buf[60]; // ?
t=0;tout=tout1;
double *x;
x=new double[9];
      i=0;j=0;
init_cond1(x, fl_in, np, xsdvig_in);

FILE *real;
//real=fopen("d:\\polovinkin\\Ml\\real.dat","w");
real=fopen("real.dat","w");
/*
if (t<tout && x_xgr<0) condition = 1;
if (unic==1)
{if ((t<tout && x_xgr<0) || (t<tout && x_xgr >= 0 &&  x[1] < x[0]-x[0]*x[0]*x[0]/3) ) condition = 1;
else condition = 0;
}*/
//while(t<tout && x[0]<xgr)
//condition=1;
//while(t<tout && x[1]>-2  && x[1]<3 && s<nCOLUMNS)
//while(t<tout && x[0]<xgr && s<nCOLUMNS || t<tout && x[1] < x[0]-x[0]*x[0]*x[0]/3 && s<nCOLUMNS)
//Zdes'
while(t<tout && s<nCOLUMNS && x[0]<xgr && x[1]> y_massivl1_appr(x[0],mssv))  
//while(x[0]<xgr && s<nCOLUMNS)

//while(condition)
{
	switch(rkf45(right_part,9,x,&t,tout,&relerr,abserr,&flag))
     {
     case 2:
     break;
     case -2:

otklon = x[1] - (x[0]-x[0]*x[0]*x[0]/3);
	if (unic==1)
	{
	vx =  Q_11*x[2] + ax(x[0],x[1],t);
	vy =  Q_22*x[3] + ay(x[0],x[1],a, ro,epsn,t);
	sq_v = sqrt(vx*vx+vy*vy);
	projection=vx*ax(x[0],x[1],t) + vy*ay(x[0],x[1],a,ro,epsn,t);
	tg[0]=ay(x[0],x[1],a,ro,epsn,t)/ax(x[0],x[1],t);
	proj_ort_xy(x[0],mssv,pr_ort_xy);
	//prj=vx*cos_alpha + vy*sin_alpha;
	prj=vx*pr_ort_xy[0] + vy*pr_ort_xy[1];
	}
//	t_out=t;
	    for(q=0;q<9;q++)
		{
		//u_2bef[q]=u_bef[q];
		u_bef[q]=u_aft[q];
		}
	u_aft[0]=x[0];
	u_aft[1]=x[1];
	u_aft[2]=x[2];
	u_aft[3]=x[3];
	u_aft[4]=x[4];
	u_aft[5]=x[5];
	u_aft[6]=Q_11*x[2] + ax(x[0],x[1],t);
	u_aft[7]=Q_22*x[3] + ay(x[0],x[1],a,ro,epsn,t);
//	u_aft[8]=prj;
//	x_xgr=x[0]-0;
//	x_xgr_bef=xgr;
	 if (unic==1)
fprintf(real,"%lg \t  %lg \t  %lg  \t  %lg  \t  %lg  \n",t,x[0],x[1],x[4],100.*sq_v);
     i++;
     j++;
/*	      	 if (unic==-1 && i==m1)
		 	 
			 {
				int s=j/m1-1;	
			
				if(x[0]<xgr && s<nCOLUMNS)
				{
				nx=2*np*nCOLUMNS+s;	
				ny=(2*np+1)*nCOLUMNS+s;
				paths[nx]=x[0]; paths[ny]=x[1];
				}
			 }
*/
/*	{
s=j/m1-1;
nx=2*np*nCOLUMNS+s;	ny=(2*np+1)*nCOLUMNS+s;
		 fprintf(real,"%i \t %i \t %lg \t %lg \t %lg \n",s,np,t,x[0],x[1]);
//		 double paths[nROWS][nCOLUMNS];
paths[nx]=x[0]; paths[ny]=x[1];
	}*/
     if (i==m1)
         i=0;
             break;
     case 5: abserr+=1e-9;
             flag=-2;
             break;
      case 6: relerr*=10;
             flag=-2;
             break;
     case 7: flag=-2;
             break;
     case 8: printf("\n Incorrect call of RKF45");
             exit(1);
     case 9: printf("\n No enough memory for RKF45");
             exit(1);
    }        
  }

free_rkf();
delete x;
fclose(real);
u_prs[0]=separ_peresech(u_bef[0],u_bef[1],u_aft[0],u_aft[1],mssv[x_knot_num],mssv[x_knot_num+SPR],mssv[x_knot_num+1],mssv[x_knot_num+SPR+1],mssv);
return u_prs[0]-x_center;
}

double current(int num_curr, int num_ini,int d_num,double Cnst, double(*f)(double,double,double(double,double*),double*,double),double *ms, double goodreltail) //Y!b
{
	y_sdvig_in=0;
	int num,diff_num=0,ns;//,minmaxnum;//, d_num=-1;
	double estm_crrnt=0,d_crrnt=1.,d_crrntx,d_crrnty,old_d_crrnt,estm_tail,tg_alph,relestm_tail=1.;
	double firstxsdvig=first_xsdvig, scndxsdvig=scnd_xsdvig,crrnt_sdvg;
	if (d_num<0) num=num_ini-1;
	else if (d_num>0) num=num_ini;
	global=fopen("global.dat","a");
	fprintf(global,"\n x_sdvig, \t \t x_prs, \t y_prs, \t vx/vy,  \t S_prs, \t  Pref_prs,\t Pr_Density, d_Pr_current, \t  estm_crrnt  \t estm_tail, relestm_tail, x_kn_num, x_cntr, u_prs[0]-x_cntr, min_nm, max_nm, pref_mssv0[QNT+num-num_ini], num_curr\n");
//	if (d_num<0) minmaxnum=boundary_num[0];
//	else if (d_num>0) minmaxnum=boundary_num[1];
	while (fabs(relestm_tail)>goodreltail && num>=minmax_bndry_num[0] && num<=minmax_bndry_num[1])//abs(diff_num)<minmaxnum)
	{
		x_center=(ms[num]+ms[num+1])/2;
		tg_alph=(ms[num+1+SPR]-ms[num+SPR])/dx;
		first_xsdvig=-0.0001348380979;//-0.000134838;//-0.0001348372788;//
		scnd_xsdvig= -0.000593409;//-0.000592;//-0.000593409-nonsdv;
		crrnt_sdvg =f(scndxsdvig,firstxsdvig,call1,ms,1.e-12);
		if (d_num<0) firstxsdvig=crrnt_sdvg;
		else if (d_num>0) scndxsdvig=crrnt_sdvg;

		for (ns=0; ns<9; ns++)
		{
			u_prs[ns]=lin1_appr(u_bef[0], u_aft[0], u_bef[ns], u_aft[ns], u_prs[0]);
		}
		double density=Cnst*u_prs[5]*exp(-u_prs[4]/b);
		old_d_crrnt=d_crrnt; 
		d_crrnt=(-u_prs[3])*density*dx/2;
		estm_tail=d_crrnt/(1.-d_crrnt/old_d_crrnt);
		estm_crrnt=estm_crrnt+d_crrnt;
		relestm_tail=estm_tail/estm_crrnt;
			
		fprintf(global,"%lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %i \t %lg \t %lg \t %i \t %i \t %lg \t %i\n",crrnt_sdvg,u_prs[0],u_prs[1],u_prs[7]/u_prs[6],u_prs[4]/b,u_prs[5],density,d_crrnt,estm_crrnt,estm_tail,relestm_tail,x_knot_num,x_center,u_prs[0]-x_center,min_num,max_num,u_prs[5]*dx,num_curr);
		if (abs(x_knot_num-num)>0)
		{
			fprintf(global,"\n Incorrect boundaries in zeroin? \n");
			printf("\n Incorrect boundaries in zeroin? \n");
			exit(1);
		}
		else 
		{
			diff_num=num-num_ini;
			if (num_curr==0)
			{
				if (d_num<0) min_num=QNT+num-num_ini;
				if (d_num>0) max_num=QNT+num-num_ini;
				pref_mssv0[QNT+num-num_ini]=Cnst*u_prs[5]*dx;
				S_mssv0[QNT+num-num_ini]=u_prs[4]/b;
//				ms_tg_alph[QNT+num-num_ini]=tg_alph;
			}
			if (num_curr==1)
			{
				pref_mssv_sdvx[QNT+num-num_ini]=Cnst*u_prs[5]*dx;
			}
			if (num_curr==2)
			{
				pref_mssv_sdvy[QNT+num-num_ini]=Cnst*u_prs[5]*dx;
			}
//			fprintf(global,"%i \t %i \t %lg \t %lg\n",diff_num,x_knot_num,pref_mssv0[QNT+diff_num]);
			if (abs(diff_num)>QNT) printf("\n abs_max_diff_num>QNT \n");
		}

		num=num+d_num;
	}

if (d_num<0 && num_curr==0) minmax_bndry_num[0]=num=num-d_num;
	else if (d_num>0 && num_curr==0) minmax_bndry_num[1]=num=num-d_num;
return estm_crrnt;
}


void main(int argc, char* argv[])
{
int knot_num,discontinuity=0;   
int knot_min=SPR, knot_max=0;//, estim_num_max=0;
double S1,S2,x_sdvig_in,S,S_old,temp0=0,temp_add=0; 
double d_current, old_d_current=1, estim_tail,x_delt_optim=1.e-12;
double estim_dens_max=0., estm_d_curr_max=0., x_sdvig_new,last_x_sdvig,x_sdvig_old,density,
first_xsdvig_new,scnd_xsdvig_new,	scnd_xsdvig_right,scnd_xsdvig_left;

 
//double sepr_sdvigx[2*SPR];
read=reading(); //CONSTANTS' CONCRETEZATION
reads=read_separ();

cov_matrix(a,ro,epsn,Q_11,Q_12,Q_22);
//acc=4.e-3;
/*teta1=teta1*pi;
teta2=teta2*pi;*/
//teta=(teta1+teta2)/2;
//d_teta=d_teta*pi;
double C=1/(2*pi*b*sqrt(detnew));
x_sdvig_in=first_xsdvig;
y_sdvig_in=0;
if (scnd__xsdvig*scnd__xsdvig < first_xsdvig*first_xsdvig) scnd_xsdvig=call0(x_sdvig_in);
else scnd_xsdvig=scnd__xsdvig;
	delta_x_in=(scnd_xsdvig-first_xsdvig)/(N_paths);

FILE *global;
global=fopen("global.dat","w");
fprintf(global,"a: %lg r: %lg  ro: %lg pi acc: %lg\n",a,r,ro,acc);
fprintf(global,"epsn: %lg b: %lg B_11: %lg B_22: %lg \n",epsn,b,B_11,B_22);
//fprintf(global,"teta, \t t_appr, \t xappr,  yappr,\t vxappr, \t vyappr,  \t Sappr, \t x_xgr_bef, \t x_xgr,  \t  prappr \n");
fprintf(global,"x_sdvig, \t \t x_prs, \t y_prs, \t vx/vy,  \t S_prs/D, \t Pref_prs,\t S_prs_mod/D, \t Pr_Dens \t dPr_curr, \t  \t p_x,\t \t estm_tail,\t \t kn_min, kn_max, est_nm_max, x_kn_nm, sepr[x_kn_nm] \n");

//fprintf(global,"teta, \t x_after, \t y_after, \t vx/vy,  \t S_after, \t \t Pref_after,\t Pr_Density \t Pr_current, \t W_11, \t \t \t W_12, \t \t \t W_22, \t  \t x_xgr_bef,\t \t x_xgr \n");

/*for (teta=teta1; teta<=teta2; teta+=d_teta) 
{
S1=call1(teta,np); 
np++;
fprintf(global,"%lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \n",teta/pi,t_aft,u_aft[0],u_aft[1],u_aft[5],u_aft[6],u_aft[5]/u_aft[6],1/tg[0],u_aft[4]/b,x_xgr_bef,x_xgr,proj_aft);
}*/

//int N_P=(teta2-teta1)/d_teta+1;

if (estim_num_max==0)
{	while(fabs(delta_x_in)>=fabs(x_delt_optim)) 
	{	
	delta_x_in=(scnd_xsdvig-first_xsdvig)/(N_paths);
	fprintf(global,"first_xsdvig: %lg scnd_xsdvig: %lg delta_x_in: %lg \n",first_xsdvig,scnd_xsdvig,delta_x_in);
		for (np=0; np<N_paths+1; np++)
		{
		x_sdvig_in=first_xsdvig+delta_x_in*np;
		x_sdvig_old=first_xsdvig+delta_x_in*(np-1);
		y_sdvig_in=0;
		//teta=teta1+np*d_teta;
		call1(x_sdvig_in,sepr);

		if(x_knot_num>knot_max) knot_max=x_knot_num;
		if(x_knot_num<knot_min) knot_min=x_knot_num;

		u_prs[0]=separ_peresech(u_bef[0],u_bef[1],u_aft[0],u_aft[1],sepr[x_knot_num],sepr[x_knot_num+SPR],sepr[x_knot_num+1],sepr[x_knot_num+SPR+1],sepr);
			for (int nn=0; nn<9; nn++)
			{
			u_prs[nn]=lin1_appr(u_bef[0], u_aft[0], u_bef[nn], u_aft[nn], u_prs[0]);
			}

			S_old=S;
			S=u_prs[4];
			if (S_old>S)
			{
			fprintf(global,"\n discontinuity? x_sdvig_in = %lg\n",x_sdvig_in);
			printf("\n discontinuity?  x_sdvig_in = %lg\n",x_sdvig_in);
				if (S_old > 2*S)
				{	
		//		fprintf(global,"%lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %i  \t %i \t %i  \t %i \t %lg \t %lg\n",x_sdvig_in,u_prs[0],u_prs[1],u_prs[7]/u_prs[6],u_prs[4]/b,u_prs[5],density,d_current,u_prs[2],estim_tail,knot_min,knot_max,estim_num_max,x_knot_num,sepr[x_knot_num],S1);
				discontinuity=1;
				x_sdvig_new=x_sdvig_in;
				last_x_sdvig=x_sdvig_old;
				}
				else 
				{
				fprintf(global,"\n delta_x_in (sdvig) too small?\n");
				printf(" delta_x_in (sdvig) too small?\n");
				}
			}
		density=C*u_prs[5]*exp(-u_prs[4]/b);
		proj_ort_xy(u_prs[0],sepr,pr_ort_xy);
		old_d_current=d_current; 
		d_current= (-u_prs[3])*density*dx/2;
//			(u_prs[6]*pr_ort_xy[0]+u_prs[7]*pr_ort_xy[1])*density*dx;
		estim_tail=d_current/(1.-d_current/old_d_current);	
		if (fabs(d_current)>estm_d_curr_max)
			{
			estm_d_curr_max=fabs(d_current);
			estim_num_max=x_knot_num;
			x_sdvg_max_dcrr=x_sdvig_in;
			}
		//fprintf(global,"%lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \n",teta/pi,t_aft,u_aft[0],u_aft[1],u_aft[5],u_aft[6],u_aft[5]/u_aft[6],1/tg[0],u_aft[4],x_xgr_bef,x_xgr,proj_aft);
		//fprintf(global,"%lg \t %lg \t %lg \t \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %d \t %lg \n",teta/pi,t_out,u_aft[0],u_aft[1],u_aft[3]/u_aft[2],u_aft[4]/b,u_aft[5],u_aft[5]*exp(-u_aft[4]/b),prj*u_aft[5]*exp(-u_aft[4]/b),u_aft[6],u_aft[7],u_aft[8],ss,x_xgr);
		fprintf(global,"%14.13lg \t %lg \t %lg \t %8.7lg \t  %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %i  \t %i \t %i  \t %i \t %lg\n",x_sdvig_in,u_prs[0],u_prs[1],u_prs[7]/u_prs[6],u_prs[4]/b,u_prs[5],u_prs[4]/b-log(u_prs[5]),density,d_current,u_prs[2],estim_tail,knot_min,knot_max,estim_num_max,x_knot_num,sepr[x_knot_num]);
		}

		if (estim_num_max>knot_min && estim_num_max<knot_max) 
		{
		fprintf(global,"\n maximum found; estim_num_max = %i x_sdvg_max_dcrr = %lg\n",estim_num_max,x_sdvg_max_dcrr);
		printf("\n maximum found \n");
		//maximum_found=1;
		}
		else
		{
		fprintf(global,"\n maximum not found \n");
		printf("\n maximum not found \n\n");
		}

		if (discontinuity) 
		{
		first_xsdvig=last_x_sdvig;
		scnd_xsdvig=x_sdvig_new;
		fprintf(global,"\n last_x_sdvig = %14.13lg \t x_sdvig_new= %14.13lg \n",first_xsdvig,scnd_xsdvig);
		scnd_xsdvig_right=call0(last_x_sdvig);
		scnd_xsdvig_left=call0(x_sdvig_new);
		fprintf(global,"\n scnd_xsdvig_right= %14.13lg scnd_xsdvig_left= %14.13lg \n",scnd_xsdvig_right,scnd_xsdvig_left);
		}
	}
	if (discontinuity)
	{
//	first_xsdvig=x_sdvig_new;
//	scnd_xsdvig=call0(last_x_sdvig);
	first_xsdvig=first_xsdvig_new;
	scnd_xsdvig=scnd_xsdvig_new;

//		fprintf(global,"\n first_xsdvig_new = %14.13lg \t scnd_xsdvig_new= %14.13lg \n",first_xsdvig,scnd_xsdvig);
	}
}/*
if (unic==-1)
{
FILE *Paths;
//Paths=fopen("d:\\polovinkin\\Ml\\Paths.dat","w");
Paths=fopen("Paths.dat","w");
while(s0<nCOLUMNS)           
	{
	fprintf(Paths,"%i \t %i" ,N_paths,s0);
	for(int p1=0; p1<N_paths; p1++) 
//if(!(paths[2*q1*nCOLUMNS+s0]==0 && paths[(2*q1+1)*nCOLUMNS+s0==0]))    
	fprintf(Paths," \t %lg \t %lg" ,paths[2*p1*nCOLUMNS+s0],paths[(2*p1+1)*nCOLUMNS+s0]);
	fprintf(Paths," \n");
	s0++;
//fprintf(global,"%i \t %i \t %lg \t %lg \t %lg \t %lg  \t %lg \t %lg \n",s0,N_P,paths[s0],paths[s1],paths[s2],paths[s3],paths[s4],paths[s5]);s0++;
	}
fclose(Paths);
}
*/


else if(estim_num_max>0)
{
double curl0,curr0,curl1,curr1,curl2,curr2;
minmax_bndry_num[0]=0, minmax_bndry_num[1]=350;

double sdvgy=-0.002;
for (int mm=0; mm<SPR; mm++) 
	{
		sepr_sdvg[mm]=sepr[mm];
	}
for (int nm=SPR; nm<2*SPR; nm++) 
	{
		sepr_sdvg[nm]=sepr[nm]+sdvgy;
	}

int N_phi=12; 
for( int m_phi = 1; m_phi <= N_phi; m_phi++)
{
	phi0=2*pi*(m_phi-1)/N_phi;	
	
printf("\n m_phi = %i\n",m_phi);
curl0=current(0,estim_num_max,-1,C,zeroinnew1,sepr,0.0025); //Y!b
//curl0=current(0,estim_num_max,-1,minmax_bndry_num,zeroinnew1,sepr,0.005); //Y!b
fprintf(global,"%i \t %i \n",minmax_bndry_num[0],minmax_bndry_num[1]);
curr0=current(0,estim_num_max,1,C,zeroinnew1,sepr,0.0025); //Y!b
//curr0=current(0,estim_num_max,1,minmax_bndry_num,zeroinnew1,sepr,0.005); //Y!b
fprintf(global,"%i \t %i  \t %lg \n",minmax_bndry_num[0],minmax_bndry_num[1],sepr_tms[10]);
temp0+=curl0+curr0;

fprintf(global,"num, prefactor,  tg_alph, \t pref_sdvy,  S_(sdg=0),   dwdy_curr,\t estmcrnt_dwy,\t estmtail_dwy,\t relestmtail_dwy\n");

//curl2=current(2,estim_num_max,-1,minmax_bndry_num,zeroinnew1,sepr,0.00005);//0.0001); //Y!b
//curr2=current(2,estim_num_max,1,minmax_bndry_num,zeroinnew1,sepr,0.00005);//0.00005); //Y!b
curl2=current(2,estim_num_max,-1,C,zeroinnew1,sepr_sdvg,0.00025);//0.0001); //Y!b
curr2=current(2,estim_num_max,1,C,zeroinnew1,sepr_sdvg,0.00025);//0.00005); //Y!b
/*double sdvgx=0.0035;
for (int mm=0; mm<SPR; mm++) 
	{
		sepr[mm]=sepr[mm]+sdvgx;
		sepr[SPR+mm]=sepr[SPR+mm]-sdvgy;
	}

//curl1=current(1,estim_num_max,-1,minmax_bndry_num,zeroinnew1,sepr,0.00005);//0.0001); //Y!b
//curr1=current(1,estim_num_max,1,minmax_bndry_num,zeroinnew1,sepr,0.00005);//0.00005); //Y!b 
curl1=current(1,estim_num_max,-1,zeroinnew1,sepr,0.00005);//0.0001); //Y!b
curr1=current(1,estim_num_max,1,zeroinnew1,sepr,0.00005);//0.00005); //Y!b 
*/
  double 	old_crnt_dwy=1.,old_crnt_dwx=1.,estm_tail_dwy,estm_tail_dwx,estm_crnt_dwy=0.,estm_crnt_dwx=0.,relestmtail_dwy,
relestmtail_dwx,dwdy_curr=1.,dwdx_curr=1.;
	for (int sj=min_num; sj<=max_num; sj++)
	{   
		old_crnt_dwy=dwdy_curr;		
		dwdy_curr= (b/2.)*( (pref_mssv_sdvy[sj]-pref_mssv0[sj])/sdvgy)*exp(-S_mssv0[sj]);
		estm_tail_dwy=dwdy_curr/(1.-dwdy_curr/old_crnt_dwy);
		estm_crnt_dwy=estm_crnt_dwy+dwdy_curr;
		relestmtail_dwy=estm_tail_dwy/estm_crnt_dwy;
//		fprintf(global,"%i \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg\n",sj,pref_mssv0[sj],pref_mssv_sdvx[sj],ms_tg_alph[sj],pref_mssv_sdvy[sj],S_mssv0[sj],S_mssv_sdvx[sj],S_mssv_sdvy[sj],dwdx_curr,dwdy_curr);
		fprintf(global,"%i \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg\n",sj,pref_mssv0[sj],ms_tg_alph[sj],pref_mssv_sdvy[sj],S_mssv0[sj],dwdy_curr,estm_crnt_dwy,estm_tail_dwy,relestmtail_dwy);
	}
			temp_add+=estm_crnt_dwy;

}
			/*	for (sj=min_num; sj<=max_num; sj++)
	{   
		old_crnt_dwx=dwdx_curr;		
//		dwdx_curr= (b/2.)*(-(pref_mssv_sdvx[sj]-pref_mssv0[sj])/sdvgx)*ms_tg_alph[sj]*exp(-S_mssv0[sj]);
		estm_tail_dwx=dwdx_curr/(1.-dwdx_curr/old_crnt_dwx);
		estm_crnt_dwx=estm_crnt_dwx+dwdx_curr;
		relestmtail_dwx=estm_tail_dwx/estm_crnt_dwx;
*/		
//		fprintf(global,"%i \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg\n",sj,pref_mssv0[sj],pref_mssv_sdvx[sj],ms_tg_alph[sj],pref_mssv_sdvy[sj],S_mssv0[sj],S_mssv_sdvx[sj],S_mssv_sdvy[sj],dwdx_curr,dwdy_curr);
		//fprintf(global,"%i \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg \t %lg\n",sj,pref_mssv0[sj],ms_tg_alph[sj],pref_mssv_sdvx[sj],S_mssv0[sj],dwdx_curr,estm_crnt_dwx,estm_tail_dwx,relestmtail_dwx);
//			}
 fclose(global);
printf("\n T_activation0 = %lg\n",N_phi/temp0);
global=fopen("global_w1.dat","w");
fprintf(global,"\n T_activation_est0 = %lg\n",N_phi/temp0);
	fprintf(global,"\n T_activation_est1 = %lg\n",N_phi/(temp0+temp_add));
	//fprintf(global,"\n T_activation_est2 = %lg\n",1/(curl0+curr0+estm_crnt_dwy+estm_crnt_dwx));
	//fprintf(global,"\n T_add = %lg\n",sepr_tms[estim_num_max]);
	//fprintf(global,"\n T_activation3 = %lg\n",1/(curl0+curr0+estm_crnt_dwy+estm_crnt_dwx)+sepr_tms[estim_num_max]);

	printf("\n T_activation1 = %lg\n",N_phi/(temp0+temp_add));
	//printf("\n T_activation2 = %lg\n",1/(curl0+curr0+estm_crnt_dwy+estm_crnt_dwx));
	printf("\n T_add = %lg\n",sepr_tms[estim_num_max]);
	//printf("\n T_activation3 = %lg\n",1/(curl0+curr0+estm_crnt_dwy+estm_crnt_dwx)+sepr_tms[estim_num_max]);

//*/
  fclose(global);
//double curr=

/*S1=call1(teta);
//S2=call2(teta);
if (x_xgr>0)
S2=zeroin(t_2bef,t_aft,call2,acc);
else
printf("x_xgr<0\n");
//}*/
}

}