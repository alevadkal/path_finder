/* ��� ��������� ��� ������������� (�� ���������� arg_ph, (�������������� � u_[0]) ��������)
(� ������� �� ���������� � ����� diff)
*/
#include "headers.h"
#include <math.h>

double diff_new(double arg_ph, double massiv[ROWS][COLS], par_intg &pring, double u_bef[10],double u_aft[10])
{
double Ddiffnew, arg_x, arg_y;
//arg_ph=arg_ph;
arg_x= lin1_appr(u_bef[0], u_aft[0], u_bef[1], u_aft[1], arg_ph);	
arg_y= lin1_appr(u_bef[0], u_aft[0], u_bef[2], u_aft[2], arg_ph);
Ddiffnew = difference(arg_ph, arg_x, arg_y, massiv, pring);

return Ddiffnew;
}

double difference(double arg_ph_,double arg_x_,double y_,double massiv[ROWS][COLS],par_intg &pring)
{
	double bound_phi=0, x_max=0;
	double step_phi= 2*pi/(ROWS -1);
	double step_x = (x_max-pring.x_min)/(COLS -1);
//    double step_lin_phi;//,  arg_x_, varbl[2];

	double arg10, arg20, y00, y10, y01, y11, Difference_;
	int Narg0, sdvig_number;

//	step_lin_phi=u_aft[0]-u_bef[0];

	if (arg_ph_ <= 2*pi) 
	{
		Narg0=number(arg_ph_,bound_phi,step_phi);
	}
	else 
	{
		sdvig_number= floor((arg_ph_-bound_phi)/(2*pi));
		arg_ph_=arg_ph_ - 2*pi*sdvig_number;
		Narg0=number(arg_ph_,bound_phi,step_phi);
	}
		
	int Narg1=number(arg_x_, pring.x_min, step_x);

	arg10=Narg0*step_phi+bound_phi;
	arg20=Narg1*step_x+pring.x_min;
	
	y00=massiv[Narg0][Narg1];
	y10=massiv[Narg0+1][Narg1];
	y01=massiv[Narg0][Narg1+1];
	y11=massiv[Narg0+1][Narg1+1];

Difference_ = y_ - appr_2D(arg10,arg20,step_phi,step_x,y00,y01,y10,y11,arg_ph_,arg_x_);
return Difference_;
}
