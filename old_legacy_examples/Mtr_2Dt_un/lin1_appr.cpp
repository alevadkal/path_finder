#include <stdio.h>
//#include <conio.h>
#include <stdlib.h>
#include <math.h>


double lin1_appr_m(double arg0, double step_arg, double fnkz0, double fnkz1, double arg)
{

	return fnkz0+(arg-arg0)*(fnkz1-fnkz0)/step_arg;
}

double lin1_appr(double arg0, double arg1, double fnkz0, double fnkz1, double arg)
{
return fnkz0+(arg-arg0)*(fnkz1-fnkz0)/(arg1-arg0);
}