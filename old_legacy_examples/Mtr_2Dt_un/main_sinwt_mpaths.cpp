#include <fstream.h>
#include <stdio.h>
#include "headers.h"
#include <math.h>

	double b, a, xgr, epsn, tout, s, x_min, ro, first_xsdvig, scnd__xsdvig, relerr,
		Q11, Q12, Q22, acc, w, A,xsdvigin, cov_mtrx[3];
    int N_paths, unic, m1, Num_phas_all, estim_num_max;

int reading(eq_par&, ini_data&, par_intg&);

int reading(eq_par &eqpar, ini_data &inda, par_intg &pring)
{
	FILE *fin,*fout;
	fin=fopen("constants_many_sinwt.txt","r");
	fout=fopen("result.txt","w");
	while (fscanf(fin,"b=%lg a=%lg xgr=%lg epsn=%lg tout=%lg x_min=%lg xsdvigin=%lg \n first_xsdvig=%lg scnd__xsdvig=%lg N_paths=%i relerr=%lg unic=%i \n m1=%i Num_phas_all=%i Q11=%lg Q22=%lg acc=%lg estim_num_max=%i w=%lg A=%lg",&b,&a,&xgr,&epsn,&tout,&x_min,&xsdvigin,&first_xsdvig,&scnd__xsdvig,& N_paths,&relerr,&unic,&m1,&Num_phas_all,&Q11,&Q22,&acc,&estim_num_max,&w,&A) != EOF) {
double c=a+b;
fprintf(fout,"%lg\n",c);	
fprintf(fout,"b: %lg a: %lg xgr: %lg epsn: %lg tout: %lg x_min: %lg xsdvigin: %lg \n first_xsdvig: %lg scnd__xsdvig: %lg  N_paths: %i \n",b,a,xgr,epsn,tout,x_min,xsdvigin,first_xsdvig,scnd__xsdvig,N_paths);
fprintf(fout,"relerr: %lg m1: %i Num_phas_all: %i Q11: %lg Q22: %lg acc: %lg unic: %i \n",relerr,m1,Num_phas_all,Q11,Q22,acc,unic);
		}
	fclose(fin);
fclose(fout);
		
		eqpar.a=a;eqpar.ro=0;eqpar.epsn=epsn;eqpar.w=w;eqpar.A=A;
		eqpar.Q11=Q11;eqpar.Q12=0;eqpar.Q22=Q22;
		
		inda.b=b;inda.xsdvigin=xsdvigin;
inda.ysdvigin=0;
		pring.tout=tout;pring.xgr=xgr;pring.relerr=relerr;pring.acc=acc;pring.x_min=x_min;
		pring.unic=unic;pring.m1=m1;pring.Num_phas_all=Num_phas_all;
return 1;
}
	
	int main(void){
	//double bound_phi=0, x_min=-1.5, x_max=0;
	//double step_phi= 2*pi/(ROWS -1);
	//double step_x = (x_max-x_min)/(COLS -1);
	double massiv[ROWS][COLS];
	eq_par eqpar;
	ini_data inda;
	par_intg pring;
	read2D(massiv, "massiv.txt");
	write2D(massiv, "massiv2.txt");

int r= reading(eqpar,inda,pring);
xy_stable(eqpar, inda);
printf("%f, %f\n",inda.x_st,inda.y_st);
r=cov_matrix(eqpar,inda);

FILE *global;
global=fopen("global.dat","w");
//fprintf(global,"a: %lg r: %lg  ro: %lg pi acc: %lg\n",a,r,ro,acc);
fprintf(global,"a: %lg epsn: %lg b: %lg Q11: %lg Q22: %lg \n",a,epsn,b,Q11,Q22);
//fprintf(global,"teta, \t t_appr, \t xappr,  yappr,\t vxappr, \t vyappr,  \t Sappr, \t x_xgr_bef, \t x_xgr,  \t  prappr \n");
//fprintf(global,"x_sdvig, \t \t x_prs, \t y_prs, \t vx/vy,  \t S_prs/D, \t Pref_prs,\t S_prs_mod/D, \t Pr_Dens \t dPr_curr, \t  \t p_x,\t \t estm_tail,\t \t kn_min, kn_max, est_nm_max, x_kn_nm, sepr[x_kn_nm] \n");


double u_bef[10];
double u_aft[10];
double u_prs[10];
double temp_sum=0, S1, z1;
for 
{
for (pring.num_phase=0; pring.num_phase<pring.Num_phas_all;pring.num_phase++)
{   eqpar.phi0=(2*pi*pring.num_phase)/pring.Num_phas_all;
	u_prs[0]=call1(eqpar,pring,inda,u_bef,u_aft,massiv);
	for (int nn=1; nn<9; nn++)
	{
	u_prs[nn]=lin1_appr(u_bef[0], u_aft[0], u_bef[nn], u_aft[nn], u_prs[0]);
	}
	if (pring.num_phase==0) 
	{
		S1=u_prs[5]; z1=u_prs[6];
	}
	//printf("%f %f %f %f\n",eqpar.phi0,u_prs[0],(u_prs[5]-S1)/inda.b,u_prs[6]/z1);
	printf("%f %f %f\n",eqpar.phi0,u_prs[0],(u_prs[5]-S1)/inda.b);
	temp_sum=temp_sum+(u_prs[6]/z1)*exp((u_prs[5]-S1)/inda.b);
}
double coeff=temp_sum/pring.Num_phas_all;
	printf("coeff = %f\n",coeff);
	}

fclose(global);



 printf("%f, %f, %f, %14.13f\n",eqpar.a,cov_mtrx[0],cov_mtrx[1],cov_mtrx[2]);
	return 0;
}


