#include "headers.h"
#include <stdio.h>
//#include <conio.h>
#include <stdlib.h>
//#include <math.h>
//#include <iostream.h>
//#include <fstream.h>
//#include <iomanip.h>
//#include <math.h>

double call1(eq_par &eqpar,par_intg &pring,ini_data &inda,double u_bf[10],double u__aft[10],double A[ROWS][COLS])
{
double t,vx,vy,sq_v,u_prs_0; 
double abserr;  
abserr=0.;//double relerr=1.e-6, x_xgr=-1.;
int flag=-1, q, q1;

t=0;
double tout=pring.tout, w=eqpar.w;
double rerr=pring.relerr,acc=pring.acc;
double *x;
x=new double[9];
double i=0,j=0;
double ph=eqpar.w*t+eqpar.phi0;
int fl1 =init_cond(x, inda);
FILE *real;
//real=fopen("d:\\polovinkin\\Ml\\real.dat","w");
real=fopen("real.dat","w");
/*
if (t<tout && x_xgr<0) condition = 1;
if (unic==1)
{if ((t<tout && x_xgr<0) || (t<tout && x_xgr >= 0 &&  x[1] < x[0]-x[0]*x[0]*x[0]/3) ) condition = 1;
else condition = 0;
}*/
//while(t<tout && x[0]<xgr)
//condition=1;
//while(t<tout && x[1]>-2  && x[1]<3 && s<nCOLUMNS)
//while(t<tout && x[0]<xgr && s<nCOLUMNS || t<tout && x[1] < x[0]-x[0]*x[0]*x[0]/3 && s<nCOLUMNS)
while(t<pring.tout && x[0]<pring.xgr && difference(ph,x[0],x[1],A,pring)>0)  
//while(x[0]<xgr && s<nCOLUMNS)

//while(condition)
{
	switch(rkf45n(right_part,6,x,&t,tout,&rerr,abserr,&flag,eqpar))
     {
     case 2:
     break;
     case -2:
ph=w*t;
//otklon = x[1] - (x[0]-x[0]*x[0]*x[0]/3);
	if (pring.unic==1)
	{
//	vx =  inda.Q11*x[2] + ax(x[0],x[1],t);
//	vy =  inda.Q22*x[3] + ay(x[0],x[1],a, ro,epsn,t);
//	sq_v = sqrt(vx*vx+vy*vy);
//	projection=vx*ax(x[0],x[1],t) + vy*ay(x[0],x[1],a,ro,epsn,t);
//	proj_ort_xy(x[0],mssv,pr_ort_xy);
//	tg[0]=ay(x[0],x[1],a,ro,epsn,t)/ax(x[0],x[1],t);
	//prj=vx*cos_alpha + vy*sin_alpha;
//	prj=vx*pr_ort_xy[0] + vy*pr_ort_xy[1];
	}
//	t_out=t;
	for(q=0;q<10;q++)
	{
		u_bf[q]=u__aft[q];
	}
	for(q1=0;q1<9;q1++)
	{
		u__aft[q1+1]=x[q1];
	}
	u__aft[0]=ph;
		/*	u__aft[0]=x[0];
	u__aft[1]=x[1];
	u__aft[2]=x[2];
	u__aft[3]=x[3];
	u__aft[4]=x[4];
	u__aft[5]=x[5];
	u__aft[6]=x[6];
	u__aft[7]=x[7];
	u__aft[8]=x[8];
*/
	//	u__aft[6]=inda.Q11*x[2] + ax(x[0],x[1],t);
//	u__aft[7]=inda.Q22*x[3] + ay(x[0],x[1],a,ro,epsn,t);
//	u__aft[8]=prj;
//	x_xgr=x[0]-0;
//	x_xgr_bef=xgr;
	 if (pring.unic==1) fprintf(real,"%lg \t  %lg \t  %lg  \t  %lg   \t  %lg  \n",t,x[0],x[1],x[4]/inda.b,x[5]);
//fprintf(real,"%lg \t  %lg \t  %lg  \t  %lg  \t  %lg  \n",t,x[0],x[1],x[4],100.*sq_v);


    i++;
    j++;
/*	      	 if (unic==-1 && i==m1)
		 	 
			 {
				int s=j/m1-1;	
			
				if(x[0]<xgr && s<nCOLUMNS)
				{
				nx=2*np*nCOLUMNS+s;	
				ny=(2*np+1)*nCOLUMNS+s;
				paths[nx]=x[0]; paths[ny]=x[1];
				}
			 }
*/
/*	{
s=j/m1-1;
nx=2*np*nCOLUMNS+s;	ny=(2*np+1)*nCOLUMNS+s;
		 fprintf(real,"%i \t %i \t %lg \t %lg \t %lg \n",s,np,t,x[0],x[1]);
//		 double paths[nROWS][nCOLUMNS];
paths[nx]=x[0]; paths[ny]=x[1];
	}*/
     if (i==pring.m1) i=0;
//             break;
     case 5: abserr+=1e-9;
             flag=-2;
             break;
      case 6: pring.relerr*=10;
             flag=-2;
             break;
     case 7: flag=-2;
             break;
     case 8: printf("\n Incorrect call of RKF45");
             exit(1);
     case 9: printf("\n No enough memory for RKF45");
             exit(1);
    }        
  }

free_rkf();
delete x;
fclose(real);

u_prs_0=  zeroin_new2b(u_bf, u__aft, diff_new, A, pring, acc);
//u_prs[0]=separ_peresech(u_bf[0],u_bf[1],u__aft[0],u__aft[1],mssv[x_knot_num],mssv[x_knot_num+SPR],mssv[x_knot_num+1],mssv[x_knot_num+SPR+1],mssv);
return u_prs_0;//-x_center;
}

/*S1=call1(teta);
//S2=call2(teta);
if (x_xgr>0)
S2=zeroin(t_2bef,t_aft,call2,acc);
else
printf("x_xgr<0\n");
//}*/
//}

//}