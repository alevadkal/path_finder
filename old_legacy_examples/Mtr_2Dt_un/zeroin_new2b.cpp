#include <stdlib.h>
#include <fstream.h>
#include <stdio.h>
#include "headers.h"
#include <math.h>
double zeroin_new2b(double ub[10],double ua[10], double f(double,double[ROWS][COLS], par_intg&,double[],double[]), double ms[ROWS][COLS], par_intg &pring, double tol)
//double zeroinnew1(double arg_phi,double massiv[ROWS][COLS], double u_bef[10], double u_aft[10])
/* ���� ������� f(x) �����������  � ��������� [ax,bx];
   ����: ax,bx - ����� � ������ ����� ��������� ���������,
	 f(x)  - �������, ���� ������� �������������,
	 tol   - �������� ����� ��������� ���������������� ����������;
   ���������� ���� � ������������ �� ����� 4*macheps*abs(x)+tol;
   ��� �������� ��������������, ��� f(ax) � f(bx) ����� ������ �����. */

      {
    double eps,tol1,a,b,c,d,e,fa,fb,fc,xm,p,q,r,s;
    int found, zleft, numb_iter=0, iterMAX=50;

    eps=1.0l;
    while ((1.0l+eps)>1.0l) eps/=2;
    a=ub[0]; b=ua[0]; fa=(*f)(a,ms,pring,ub,ua); fb=(*f)(b,ms,pring,ub,ua);
    zleft=1;
    do
     {
      if(zleft) { c=a; fc=fa; e=d=b-a;}
      if (fabs(fc)<fabs(fb)) { a=b; b=c; c=a; fa=fb; fb=fc; fc=fa;}
      tol1=2.0l*eps*fabs(b)+0.5l*tol;
      xm=(c-b)/2;
      found=(fabs(xm)<=tol1)||(fb==0.0l);
      if (!found)
       {
    if ((fabs(e)>=tol1)&&(fabs(fa)>fabs(fb)))
     {
      if (a==c)
       {
        s=fb/fa;
        p=2.0l*xm*s;
        q=1.0l-s;
       }
      else
       {
        q=fa/fc; r=fb/fc; s=fb/fa;
        p=s*(2.0l*xm*q*(q-r)-(b-a)*(r-1.0l));
        q=(q-1.0l)*(r-1.0l)*(s-1.0l);
       }
      if (p>0.0l) q=-q; else p=-p;
      if ((2.0l*p>=(3.0l*xm*q-fabs(tol1*q)))||(p>=fabs(0.5l*e*q))) e=d=xm;
      else { e=d; d=p/q;}
     }
    else e=d=xm;
    a=b; fa=fb;
    if (fabs(d)>tol1) b+=d; else if (xm>0.0l) b+=tol1; else b-=tol1;
    fb=(*f)(b,ms,pring,ub,ua);
    zleft=fb*(fc/fabs(fc))>0.0l;
	  }  numb_iter=numb_iter+1;
     }
    while (!found && (numb_iter<iterMAX));    // To calculate phi_zeroin.
    if ( numb_iter>=iterMAX )
    {
    b = drob_otr(ms,pring,ub, ua, c);
    }
   	printf("numb_iter= %i\n",numb_iter);
	return (b);
   }

