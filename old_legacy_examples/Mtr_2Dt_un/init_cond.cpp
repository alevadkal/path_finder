#include <stdio.h>
#include <math.h>
#include "headers.h"

double S_lin(double z,double w,ini_data &inda) 
{
return 0.5*(inda.Dy*z*z-2*inda.Bxy*z*w+inda.Dx*w*w)/inda.detnw;
}

int init_cond(double *x, ini_data &inda)
{
    double S_0,rel_dev_x,rel_dev_y,del_boundr_x,del_boundr_y;
	double v0x,v0y,vS0,xa,ya,x_in,y_in;
int fl1=1; // ?


//teta=(teta1+teta2)/2;
/*double x_new=r*sigm_X_ellips*cos(teta);
double y_new=r*sigm_Y_ellips*sin(teta);
double x_in=x_new*cos(gamma)-y_new*sin(gamma);
double y_in=x_new*sin(gamma)+y_new*cos(gamma);*/

rel_dev_x=inda.xsdvigin/inda.sigm_X; rel_dev_y=inda.ysdvigin/inda.sigm_Y;
//Det=detnew;
S_0=S_lin(inda.xsdvigin,inda.ysdvigin,inda);
//S_0= 0.5*(Dy*x_in*x_in-2*Bxy*x_in*y_in+Dx*y_in*y_in)/detnew;

x[0]= inda.x_st+inda.xsdvigin;
x[1]= inda.y_st+inda.ysdvigin;
x[2]= (inda.xsdvigin*inda.Dy-inda.ysdvigin*inda.Bxy)/inda.detnw;
x[3]= (inda.ysdvigin*inda.Dx-inda.xsdvigin*inda.Bxy)/inda.detnw;
x[4]= S_0; 
x[5]= 1;
x[6]= inda.Dy/inda.detnw; 
x[7]= -inda.Bxy/inda.detnw; 
x[8]= inda.Dx/inda.detnw;      


FILE *inpt;
inpt=fopen("inpt.dat","a");
//if(np>=0 && np < 5)
//{
//      fprintf(inpt,"np: %i first_xsdvig: %lg: scnd_xsdvig: %lg: x_sdvig_in: %lg y_sdvig_in: %lg \n",np,first_xsdvig,scnd_xsdvig,xsdvigin,y_sdvig_in);      
//      fprintf(inpt,"np: %i xsdvigin: %lg ysdvigin: %lg \n",np,inda.xsdvigin,inda.ysdvigin);      
	  fprintf(inpt,"rel_dev_x: %lg rel_dev_y: %lg  S_0: %lg \n",rel_dev_x,rel_dev_y,S_0);
//      fprintf(inpt,"del_boundr_x: %lg del_boundr_y: %lg \n",del_boundr_x,del_boundr_y);
//      fprintf(inpt,"rel_del_boundr_x: %lg rel_del_boundr_y: %lg \n",del_boundr_x/sigm_X,del_boundr_y/sigm_Y);

	  fprintf(inpt,"x_0=: %10.9lg\n",x[0]);
      fprintf(inpt,"y_0=: %10.9lg\n",x[1]); 
      fprintf(inpt,"px_0=: %lg\n",x[2]);
      fprintf(inpt,"py_0=: %lg\n",x[3]);
      fprintf(inpt,"S_0/inda.b=: %lg\n",x[4]/inda.b);
	  fprintf(inpt,"z_0=: %lg\n",x[5]); 
      fprintf(inpt,"w11_0=: %lg\n",x[6]);
      fprintf(inpt,"w12_0=: %lg\n",x[7]);
      fprintf(inpt,"w22_0=: %lg\n",x[8]);
      
//	  fprintf(inpt,"vx_0=: %lg\n",v0x);
//      fprintf(inpt,"vy_0=: %lg\n",v0y);
//     fprintf(inpt,"vS_0=: %lg\n",vS0);
//      fprintf(inpt,"ax_0=: %lg\n",xa);
//      fprintf(inpt,"ay_0=: %lg\n",ya);
	  fclose(inpt);
//}

return fl1;
}
