#include <stdio.h>
//#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include "headers.h"


/*
double x_stable(double v, double s)
{
/*double kor3,kor2;
kor2 = sqrt(6561*v*v*pow(s,4.)-2916.*pow((-1.+s),3.)*s*s*s);
kor3 = pow((-81*v*s*s+kor2), 1./3.);
	return (3.*pow(2.,1./3.)*(-1.+s))/kor3+kor3/(3*pow(2.,1./3.)*s);*/
/*	return -v;
}

double y_stable(double v1,double s1)
{
/*double x_o;
x_o=x_stable(v1,s1);
	return (x_o+v1)/s1;*/
/*	return -v1+v1*v1*v1/3;
}

*/
void xy_stable(eq_par &eqpar, ini_data &inda)
{
double v;
v=eqpar.a;
/*double kor3,kor2;
kor2 = sqrt(6561*v*v*pow(s,4.)-2916.*pow((-1.+s),3.)*s*s*s);
kor3 = pow((-81*v*s*s+kor2), 1./3.);
	return (3.*pow(2.,1./3.)*(-1.+s))/kor3+kor3/(3*pow(2.,1./3.)*s);*/
//	return -v;
	inda.x_st= -v;
/*double x_o;
x_o=x_stable(v1,s1);
	return (x_o+v1)/s1;*/
//	return -v1+v1*v1*v1/3;
	inda.y_st= -v+v*v*v/3;
}

double ax(double x, double y, double t, eq_par &eqpar)
{
       return x-x*x*x/3-y;
}
double ax_x(double x, double y, double t, eq_par &eqpar)
{
               return 1-x*x;
}
double ax_y(double x, double y, double t, eq_par &eqpar)
{
        return -1;
}
double ay(double x, double y, double t, eq_par &eqpar)
{
//	double a,ro,epsn;
//	a=eqpar.a,ro=eqpar.ro,epsn=eqpar.epsn;
        return  eqpar.epsn*(eqpar.a+x-eqpar.ro*y);   
}
double ay_x(double x, double y, double t, eq_par &eqpar)
{
//    double epsn;
//	epsn=eqpar.epsn;    
	return eqpar.epsn;
}
double ay_y(double x, double y, double t, eq_par &eqpar)
{
//	double epsn,ro;
//	epsn=eqpar.epsn,ro=eqpar.ro;
        return -eqpar.epsn*eqpar.ro;
}
double ax_xx(double x, double y, double t, eq_par &eqpar)
{
               return -2*x;
}
double ax_xy(double x, double y, double t, eq_par &eqpar)
{
               return 0;
}
double ax_yy(double x, double y, double t, eq_par &eqpar)
{
               return 0;
}
double ay_xx(double x, double y, double t, eq_par &eqpar)
{
        return 0;
}
double ay_xy(double x, double y, double t, eq_par &eqpar)
{
        return 0;
}

double ay_yy(double x, double y, double t, eq_par &eqpar)
{
        return 0;
}

void right_part(double t,double *x,double *xp, eq_par &eqpar)
//void right_part(double t,double *x,double *xp,double *Q,double *param_eq)
{      
//double a,ro,epsn,A,w,phi0;//,Q11,Q12,Q22;
//a=eqpar.a;ro=eqpar.ro;epsn=eqpar.epsn;A=eqpar.A;w=eqpar.w;phi0=eqpar.phi0;
//Q11=eqpar.Q11;Q12=eqpar.Q12;Q22=eqpar.Q22;
		xp[0]=  eqpar.Q11*x[2] + ax(x[0],x[1],t,eqpar)+eqpar.A*sin(eqpar.w*t+eqpar.phi0);	
	//xp[0]=  eqpar.Q11*x[2] + ax(x[0],x[1],t,eqpar);      
        xp[1]=  eqpar.Q22*x[3] + ay(x[0],x[1],t,eqpar);
        xp[2]= -x[2]*ax_x(x[0],x[1],t,eqpar)-x[3]*ay_x(x[0],x[1],t,eqpar);
        xp[3]= -x[2]*ax_y(x[0],x[1],t,eqpar)-x[3]*ay_y(x[0],x[1],t,eqpar);
      //  xp[4]=	x[2]*(Q11*x[2]+ax(x[0],x[1],t))+x[3]*(Q22*x[3]+ay(x[0],x[1],a,ro,epsn,t));
	  xp[4]=	0.5*eqpar.Q11*x[2]*x[2]+0.5*eqpar.Q22*x[3]*x[3];//+eqpar.A*sin(eqpar.w*t+eqpar.phi0)*x[2];     //
        xp[5]= -(ax_x(x[0],x[1],t,eqpar)+ay_y(x[0],x[1],t,eqpar))*x[5];
/*			   +0.5*(eqpar.Q11*x[6]+eqpar.Q22*x[8])+eqpar.Q12*x[7])*x[5];
        xp[6]= -x[2]*ax_xx(x[0],x[1],t,eqpar)-x[3]*ay_xx(x[0],x[1],t,eqpar)
			   -2*x[6]*ax_x(x[0],x[1],t,eqpar)-2*x[7]*ay_x(x[0],x[1],t,eqpar)
			   -x[6]*x[6]*eqpar.Q11-2*x[6]*x[7]*eqpar.Q12-x[7]*x[7]*eqpar.Q22;
			   -x[6]*ax_y(x[0],x[1],t,eqpar)-x[7]*ay_y(x[0],x[1],t,eqpar)
			   -x[7]*ax_x(x[0],x[1],t,eqpar)-x[8]*ay_x(x[0],x[1],t,eqpar);
		xp[7]= -x[2]*ax_xy(x[0],x[1],t,eqpar)-x[3]*ay_xy(x[0],x[1],t,eqpar)
			   -x[6]*x[7]*eqpar.Q11-x[7]*x[7]*eqpar.Q12
			   -x[6]*x[8]*eqpar.Q12-x[7]*x[8]*eqpar.Q22;
		xp[8]= -x[2]*ax_yy(x[0],x[1],t,eqpar)-x[3]*ay_yy(x[0],x[1],t,eqpar)
			   -2*x[7]*ax_y(x[0],x[1],t,eqpar)-2*x[8]*ay_y(x[0],x[1],t,eqpar)
			   -x[7]*x[7]*eqpar.Q11-2*x[7]*x[8]*eqpar.Q12-x[8]*x[8]*eqpar.Q22;

*/
}

/*double qupotential(double x,double y,double Q11,double Q22)
{
return (x*x*x*x/4.-x*x/2.+0.25)/(0.5*Q11) + (y*y/2.)/(0.5*Q22);
}*/  
