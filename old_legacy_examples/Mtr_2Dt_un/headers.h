#include "fstream.h"
//#include "stdio.h"

#define pi 3.141596
#define ROWS 21
#define COLS 151

//#define ROWS 5
//#define COLS 5

struct eq_par
{
double a,ro,epsn,w,A,phi0,Q11,Q12,Q22;
};

struct ini_data
{
double b,Dx,Bxy,Dy,detnw,x_st,y_st,sigm_X,sigm_Y,xsdvigin,ysdvigin;
};

struct par_intg
{
double tout,xgr,relerr,acc,x_min;
int unic, m1, num_phase, Num_phas_all;
};

void read2D(double A[ROWS][COLS], const char *fname);
//void read2D(double [][], const char *);
//void write2D(double [][], const char *);

void write2D(double x[ROWS][COLS], const char *fname);
//int number(double, double, double);
int reading(void);
void xy_stable(eq_par&, ini_data&);
int cov_matrix(eq_par&, ini_data&);
double S_lin(double z,double w,ini_data&);
int number(double arg, double left_bound, double step_arg);
double lin1_appr_m(double arg0, double step_arg, double fnkz0, double fnkz1, double arg);
double lin1_appr(double arg0, double arg1, double fnkz0, double fnkz1, double arg);
double appr_2D(double arg10,double arg20,double step_arg1,double step_arg2,double fnkz00,double fnkz01,double fnkz10,double fnkz11,double new_arg1,double new_arg2);
double diff_new(double arg_ph,double massiv[ROWS][COLS],par_intg&,double u_bef[10],double u_aft[10]);
double difference(double arg_ph_,double arg_x_,double y_,double massiv[ROWS][COLS],par_intg&);
//double difference(double varbl_[3], double massiv[ROWS][COLS]);
double zeroin_new2b(double ub[10],double ua[10], double f(double,double[ROWS][COLS], par_intg&, double[],double[]), double ms[ROWS][COLS],par_intg&, double tol);
double drob_otr(double massiv[ROWS][COLS], par_intg&,double u_bef[10],double u_aft[10], double phi_zeroin);
int init_cond(double *x, ini_data &inda);
double ax(double x, double y, double t, eq_par&);
double ax_x(double x, double y, double t, eq_par&);
double ax_y(double x, double y, double t, eq_par&);
double ay(double x, double y, double t, eq_par&);
double ay_x(double x, double y, double t, eq_par&);
double ay_y(double x, double y, double t, eq_par&);
double ax_xx(double x, double y, double t, eq_par&);
double ax_xy(double x, double y, double t, eq_par&);
double ax_yy(double x, double y, double t, eq_par&);
double ay_xx(double x, double y, double t, eq_par&);
double ay_xy(double x, double y, double t, eq_par&);
double ay_yy(double x, double y, double t, eq_par&);
void right_part(double t,double *x,double *xp, eq_par&);
//int rkf45(void(*)(double,double*,double*),int,double*,double*,double,
//                  double*,double,int*);
int rkf45n(void(*)(double,double*,double*,eq_par&),int,double*,double*,double,
                  double*,double,int*,eq_par&);
void free_rkf(void);
double call1(eq_par&,par_intg&,ini_data&,double u_bef[10],double u_aft[10],
 double A[ROWS][COLS]);
