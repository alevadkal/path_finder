//#include <iostream.h>
//#include <fstream.h>
#include <stdio.h>
#include <math.h>
#include "headers.h"

int cov_matrix(eq_par &eqpar, ini_data &inda)
{
double a,ro,epsn,Q11,Q12,Q22,b;
double f11,f12,f21,f22,f11_analyt,f12_analyt,f21_analyt,f22_analyt;
double D_x,B_xy,D_y,detnew, D_x_analyt, B_xy_analyt, D_y_analyt;
	a=eqpar.a;ro=eqpar.ro;epsn=eqpar.epsn;
    Q11=eqpar.Q11;Q12=eqpar.Q12;Q22=eqpar.Q22;
	b=inda.b;

f11 = ax_x(inda.x_st,inda.y_st,0,eqpar);
f12 = ax_y(inda.x_st,inda.y_st,0,eqpar);
f21 = ay_x(inda.x_st,inda.y_st,0,eqpar);
f22 = ay_y(inda.x_st,inda.y_st,0,eqpar);
printf("f11: %lg f12: %lg f21: %lg f22: %lg \n",f11,f12,f21,f22);

   
	double det=2*(-f21*f22*f12+f11*f11*f22+f22*f22*f11-f12*f21*f11);
 
	D_x=(-f12*f12*Q22+2*f12*f22*Q12-Q11*f11*f22-  
                Q11*f22*f22+Q11*f12*f21)/det;
	B_xy=(f21*f22*Q11+f12*f11*Q22-2*f22*f11*Q12)/det;
	D_y=(-f21*f21*Q11+2*f21*f11*Q12+f21*f12*Q22
                -f11*f11*Q22-f22*f11*Q22)/det;

f11_analyt = 1-a*a;
f12_analyt = -1;
f21_analyt = epsn;
f22_analyt = 0;

D_x_analyt=b/(2*epsn*(a*a-1));
B_xy_analyt=-b/(2*epsn);
D_y_analyt=b*((a*a-1)*(a*a-1)+epsn)/(2*epsn*(a*a-1));

double sigm_X=sqrt(b*D_x); double sigm_Y=sqrt(b*D_y); 
double r_xy=(B_xy/(sqrt(D_x*D_y)));
detnew=(D_x*D_y-B_xy*B_xy);


/*
gamma=0.5*atan((2*B_xy)/(D_x-D_y));
sigm_X=sqrt(b*D_x); sigm_Y=sqrt(b*D_y); 
double r_xy=(B_xy/(sqrt(D_x*D_y)));
detnew=(D_x*D_y-B_xy*B_xy);
double Sqr_tmp=sqrt((D_x-D_y)*(D_x-D_y)+4*B_xy*B_xy);
sigm_X_ellips=sqrt(2*detnew/(D_x+D_y-Sqr_tmp));
sigm_Y_ellips=sqrt(2*detnew/(D_x+D_y+Sqr_tmp));
*/
/*
double bndary_X=x_st-x_massivl1_appr(y_st, sepr);
double bndary_Y=y_st-y_massivl1_appr(x_st,sepr);
double bndr_sgm_X=bndary_X/sigm_X;
double bndr_sgm_Y=bndary_Y/sigm_Y;
*/


FILE *inpt;
inpt=fopen("inpt.dat","w");

      fprintf(inpt,"a: %lg  ro: %lg\n",a,ro);      
	  fprintf(inpt,"x_st: %lg y_st: %lg \n",inda.x_st,inda.y_st);
      fprintf(inpt,"epsn: %lg b: %lg Q11: %lg Q22: %lg \n",epsn,b,Q11,Q22);
	  fprintf(inpt,"Q11: %lg Q12: %lg Q22: %lg \n",Q11,Q12,Q22);
      fprintf(inpt,"f11_analyt: %lg f12_analyt: %lg f21_analyt: %lg f22_analyt: %lg \n",f11_analyt,f12_analyt,f21_analyt,f22_analyt);
      fprintf(inpt,"f11: %lg f12: %lg f21: %lg f22: %lg \n",f11,f12,f21,f22);
	  fprintf(inpt,"Det: %lg D_x: %lg B_xy: %lg D_y: %lg \n",b*detnew,b*D_x,b*B_xy,b*D_y);
	  fprintf(inpt,"D_x_analyt: %lg B_xy_analyt: %lg D_y_analyt: %lg \n",D_x_analyt,B_xy_analyt,D_y_analyt);
      fprintf(inpt,"sigm_X: %lg sigm_Y: %lg r_xy: %lg \n",sigm_X,sigm_Y,r_xy);  
//      fprintf(inpt,"sigm_X_ellips: %lg sigm_Y_ellips: %lg gamma: %lg \n",sigm_X_ellips*sqrt(b),sigm_Y_ellips*sqrt(b),gamma);  
//	  fprintf(inpt,"bndary_X: %lg bndary_Y: %lg bndr_sgm_X: %lg bndr_sgm_Y: %lg \n",bndary_X,bndary_Y,bndr_sgm_X,bndr_sgm_Y);
	  fclose(inpt);
	  return 1;
}
