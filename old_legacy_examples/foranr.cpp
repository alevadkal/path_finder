//Эта программа разработана под "Watcom 10"
/*
This program simulates  the ONE-dimensional arrays of <n_e> Chua's circuits coupled by diffusive
coupling. A local element has piece--wise linear
nonlinearity and contains the following parameters:
< alfa, betta, gamma, m0, m1, m2 >. <d> is coupling parameter.
Time step in Runge-Kutta routine is AUTOMATIC.
It enables to simulates the pulses or fronts whose profiles should be contained in the
file <real_h.dat>
*/

#include <string>
#include <iostream>
#include <cmath>

int gmx,gmy,n_e,n_e2;
float alfa,betta,gamma1,d,m0,m1,m2;
float hx,k1,k0,k2;
std::string ini_file;
double pi,h2,h_d;
void descr(void);
void free_rkf(void);
int init_cond(double *,int);
int rkf45(void(*)(double,double*,double*),int,double*,double*,double,
          double*,double,int*);

double fx(double x)
{
    double ret;
    if(x<=-1) ret=(m1-1)*x+m1-m0;
    else if(x>=1) ret=(m2-1)*x-m2+m0;
    else ret=-(1-m0)*x;
    return ret;
}

void right_part(double t,double *x,double *xp)
{
    (void)t;
    int i,iy,iz;
    for(i=1; i<n_e-1; i++)
    {
        iy=i+n_e;
        iz=iy+n_e;
        xp[i]=alfa*(x[iy]-x[i]-fx(x[i]))+d*(x[i-1]-2*x[i]+x[i+1]);
        xp[iy]=x[i]-x[iy]+x[iz];
        xp[iz]=-betta*x[iy]-gamma1*x[iz];
    }
// ***********************
    i=0;
    iy=i+n_e;
    iz=iy+n_e;
    xp[i]=alfa*(x[iy]-x[i]-fx(x[i]))+d*(-x[i]+x[i+1]);
//   xp[i]=alfa*(x[iy]-x[i]-fx(x[i]))+d*(x[n_e-1]-2*x[i]+x[i+1]);
    xp[iy]=x[i]-x[iy]+x[iz];
    xp[iz]=-betta*x[iy]-gamma1*x[iz];

// **************************
    i=n_e-1;
    iy=i+n_e;
    iz=iy+n_e;
    xp[i]=alfa*(x[iy]-x[i]-fx(x[i]))+d*(x[i-1]-x[i]);
//   xp[i]=alfa*(x[iy]-x[i]-fx(x[i]))+d*(x[i-1]-2*x[i]+x[0]);
    xp[iy]=x[i]-x[iy]+x[iz];
    xp[iz]=-betta*x[iy]-gamma1*x[iz];
}

void right_part_unidir(double t,double *x,double *xp)
{
    int i,iy,iz;
    for(i=0; i<n_e-1; i++)
    {
        iy=i+n_e;
        iz=iy+n_e;
        xp[i]=alfa*(x[iy]-x[i]-fx(x[i]))+d*(x[i+1]-x[i]);
        xp[iy]=x[i]-x[iy]+x[iz];
        xp[iz]=-betta*x[iy]-gamma1*x[iz];
    }
// **************************
    i=n_e-1;
    iy=i+n_e;
    iz=iy+n_e;
    xp[i]=alfa*(x[iy]-x[i]-fx(x[i]))+d*(x[0]-x[i]);
    xp[iy]=x[i]-x[iy]+x[iz];
    xp[iz]=-betta*x[iy]-gamma1*x[iz];
}






int main(int argc, char **argv)
{
    int fl_in,flag_write=0,n,flag_el=0,flag_direc=0;
    int n_el=0,numb_el=10;
    n=1;
    float per=1.;
    int i,j,numb_e=1;
    m0=-0.5;
    m1=3.;
    m2=1.2;
    hx=3;
    switch(argc)
    {
    case 8:
        {
            int paramNumber = 1;
            d = std::stold(argv[paramNumber++]);
            alfa = std::stold(argv[paramNumber++]);
            per = std::stold(argv[paramNumber++]);
            n_e = std::stold(argv[paramNumber++]);
            fl_in = std::stold(argv[paramNumber++]);
            h_d = std::stold(argv[paramNumber++]);
            ini_file = argv[paramNumber++];
            break;
        }
    default:
        {
            printf("Wrong arguments");
            return -1;
        }
    }
    betta=0.5;
    gamma1=0.01;
    pi=3.141596;
    double t,t_0,tout,dt;
    double abserr,relerr;
    k0=gamma1/(gamma1+betta)-m0;
    k1=((m1-1)*gamma1+m1*betta)/(gamma1+betta);
    k2=((m2-1)*gamma1+m2*betta)/(gamma1+betta);
    n_e2=n_e*2;
    abserr=0.,relerr=1.e-6;
    int flag=-1;
    char buf[60];
    dt=10000;
    t=0;
    tout=dt;
    float *xt1,*yt1;
    xt1=new float[n_e];
    yt1=new float[n_e];
    double *x;
    x=new double[n_e*3];
    if(init_cond(x,fl_in)==0) exit(1);
    i=0;
    int iy;
    for(j=n_el*n_e; j<n_e*n_el+n_e; j++)
    {
        yt1[i]=x[j];
        xt1[i]=(float)i;
        i++;
    }
    FILE *real;
    real=fopen("real.dat","wt");
    char ch='y';
    int ind1=0;
    int ind2=-1;
    while(ch!='q')
    {
        switch(rkf45(right_part,n_e*3,x,&t,tout,&relerr,abserr,&flag))
        {
        case 2:
            break;
        case -2:
            break;
        case 5:
            abserr+=1e-9;
            flag=-2;
            break;
        case 6:
            relerr*=10;
            flag=-2;
            break;
        case 7:
            flag=-2;
            break;
        case 8:
            printf("\n Incorrect call of RKF45");
            exit(1);
        case 9:
            printf("\n No enough memory for RKF45");
            exit(1);
        }
    }
    free_rkf();
    fclose(real);
    FILE *outt;
    outt=fopen("outt.dat","wt");
    for(i=0; i<n_e; i++)
    {
        fprintf(outt,"%i %lf %lf %lf \n",i,x[i],x[i+n_e],x[i+n_e*2]);
    }
    fclose(outt);
    delete x;
    delete xt1;
    delete yt1;
}

int init_cond(double *x,int fl_in)
{
    int fl1=1;
    int i,iy,iz;
    FILE *sol;
    FILE *ini_bin;
    switch(fl_in)
    {
    case 0:
        for(i=0; i<n_e; i++)
        {
            iy=i+n_e;
            iz=iy+n_e;
            x[i]=2*cos(2*pi*hx*i/n_e);
            x[iy]=-0.8*x[i];
            x[iz]=1.5*x[i];
        }
        break;
    case 1:
        for(i=0; i<n_e; i++)
        {
            iy=i+n_e;
            iz=iy+n_e;
            x[i]=-(k0+k1)/k1;
            x[iy]=x[i]/(1+betta/gamma1);
            x[iz]=-(betta/gamma1)*x[iy];
        }
        for(i=140; i<160; i++)
        {
            iy=i+n_e;
            iz=iy+n_e;
            x[i]=(k0+k2)/k2;
            x[iy]=x[i]/(1+betta/gamma1);
            x[iz]=-(betta/gamma1)*x[iy];
        }
        break;
    case 2:
        for(i=0; i<n_e; i++)
        {
            iy=i+n_e;
            iz=iy+n_e;
            x[i]=-(k0+k1)/k1;
            x[iy]=x[i]/(1+betta/gamma1);
            x[iz]=-(betta/gamma1)*x[iy];
        }
        for(i=40; i<n_e; i++)
        {
            iy=i+n_e;
            iz=iy+n_e;
            x[i]=(k0+k2)/k2;
            x[iy]=x[i]/(1+betta/gamma1);
            x[iz]=-(betta/gamma1)*x[iy];
        }
        break;
    case 3:
        double xf,yf,zf;
        d=d/(h_d*h_d);
        sol=fopen("real_h.dat","rt");
        for(i=0; i<n_e; i++)
        {
            fscanf(sol,"%lf %lf %lf",&xf,&yf,&zf);
            iy=i+n_e;
            iz=iy+n_e;
            x[i]=xf;
            x[iy]=yf;
            x[iz]=zf;
        }
        break;
    case 4:
        d=d/(h_d*h_d);
        ini_bin=fopen(ini_file.c_str(),"rb");
        fread(x,sizeof(double),3*n_e,ini_bin);
        fclose(ini_bin);
    case 5:
        alfa=h_d*alfa/(h_d+2*d);
        d=2*d/(h_d*(h_d+2*d));
        ini_bin=fopen(ini_file.c_str(),"rb");
        fread(x,sizeof(double),3*n_e,ini_bin);
        fclose(ini_bin);

    }
    fclose(sol);
    FILE *inpt;
    inpt=fopen("inpt.dat","wt");
    for(i=0; i<n_e; i++)
    {
        fprintf(inpt,"%i %lf \n",i,x[i]);
    }
    fclose(inpt);
    return fl1;
}

