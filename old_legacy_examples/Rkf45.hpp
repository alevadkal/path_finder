#ifndef RKF45_HPP_INCLUDED
#define RKF45_HPP_INCLUDED

int rkf45(void (*f)(double,double*,double*),
          int neqn,
          double *y,
          double *tin,
          double tout,
          double  *relerr,
          double abserr,
          int *flag
         );

void free_rkf(void);

#endif // RKF45_HPP_INCLUDED
