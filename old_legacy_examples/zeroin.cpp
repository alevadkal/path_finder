
#include <math.h>

///
/// Нуль функции f(x) вычисляется в интервале [ax,bx];
/// вход: ax,bx - левый и правый конец исходного интервала,
/// f(x) - функция, нуль которой разыскивается,
/// tol - желаемая длина интервала неопределенности результата;
/// возвращает нуль с погрешностью не более 4*macheps*abs(x) tol;
/// без проверки предполагается, что f(ax) и f(bx) имеют разные знаки.
///
double zeroin(double ax, double bx, double (*f)(double), double tol)
{
    double eps,tol1,a,b,c,d,e,fa,fb,fc,xm,p,q,r,s;
    int found,zleft;

    eps=1.0l;
    while ((1.0l+eps)>1.0l) eps/=2;
    a=ax;
    b=bx;
    fa=(*f)(a);
    fb=(*f)(b);
    zleft=1;
    do
    {
        if(zleft)
        {
            c=a;
            fc=fa;
            e=d=b-a;
        }
        if (fabs(fc)<fabs(fb))
        {
            a=b;
            b=c;
            c=a;
            fa=fb;
            fb=fc;
            fc=fa;
        }
        tol1=2.0l*eps*fabs(b)+0.5l*tol;
        xm=(c-b)/2;
        found=(fabs(xm)<=tol1)||(fb==0.0l);
        if (!found)
        {
            if ((fabs(e)>=tol1)&&(fabs(fa)>fabs(fb)))
            {
                if (a==c)
                {
                    s=fb/fa;
                    p=2.0l*xm*s;
                    q=1.0l-s;
                }
                else
                {
                    q=fa/fc;
                    r=fb/fc;
                    s=fb/fa;
                    p=s*(2.0l*xm*q*(q-r)-(b-a)*(r-1.0l));
                    q=(q-1.0l)*(r-1.0l)*(s-1.0l);
                }
                if (p>0.0l) q=-q;
                else p=-p;
                if ((2.0l*p>=(3.0l*xm*q-fabs(tol1*q)))||(p>=fabs(0.5l*e*q))) e=d=xm;
                else
                {
                    e=d;
                    d=p/q;
                }
            }
            else e=d=xm;
            a=b;
            fa=fb;
            if (fabs(d)>tol1) b+=d;
            else if (xm>0.0l) b+=tol1;
            else b-=tol1;
            fb=(*f)(b);
            zleft=fb*(fc/fabs(fc))>0.0l;
        }
    }
    while (!found);
    return (b);
}
