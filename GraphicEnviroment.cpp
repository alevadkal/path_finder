
#define GL3_PROTOTYPES 1
#include <GL/glew.h>
#include "GraphicEnviroment.hpp"
#include "logger.hpp"


tGraphicEnviroment& tGraphicEnviroment::instance()
{
    static tGraphicEnviroment enviroment;
    return enviroment;
}

tGraphicEnviroment::tGraphicEnviroment()
{
    log_fun("");
    if(init() == false)
    {
        throw std::exception();
    }
}

tGraphicEnviroment::~tGraphicEnviroment()
{
    log_fun("");
    cleanup();
}

SDL_Window* tGraphicEnviroment::getWindow()
{
    log_fun("");
    return m_MainWindow;
}

SDL_GLContext tGraphicEnviroment::getGlContext()
{
    return m_MainContext;
}

bool tGraphicEnviroment::setOpenGLAttributes()
{
    log_fun("");
    // Set our OpenGL version.
    // SDL_GL_CONTEXT_CORE gives us only the newer version, deprecated functions are disabled
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    // 3.2 is part of the modern versions of OpenGL, but most video cards whould be able to run it
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

    // Turn on double buffering with a 24bit Z buffer.
    // You may need to change this to 16 or 32 for your system
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);

    // Сглаживание точек
    glEnable(GL_POINT_SMOOTH);
    glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
    // Сглаживание линий
    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    // Сглаживание полигонов
    glEnable(GL_POLYGON_SMOOTH);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);

    SDL_GL_SetSwapInterval(1);
    return true;
}

bool tGraphicEnviroment::init()
{
    log_fun("");
    if(m_IsInit) return true;
    // Initialize SDL's Video subsystem
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        log_error("Failed to init SDL" << SDL_GetError());
        return false;
    }


    do
    {
        // Create our window centered at 512x512 resolution
        m_MainWindow = SDL_CreateWindow(PROGRAM_NAME, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                        1024, 1024, SDL_WINDOW_OPENGL);
        // Check that everything worked out okay
        if (!m_MainWindow)
        {
            log_error("Unable to create window" << SDL_GetError());
            break;
        }
        do
        {
            // Create our opengl context and attach it to our window
            m_MainContext = SDL_GL_CreateContext(m_MainWindow);
            if(m_MainContext == nullptr)
            {
                log_error("SDL_GL_CreateContext" << SDL_GetError());
            }
            do
            {
                if(setOpenGLAttributes() != true)
                {
                    log_error("can't set GL atributes!");
                    break;
                }

                // This makes our buffer swap syncronized with the monitor's vertical refresh

                // Init GLEW
                // Apparently, this is needed for Apple. Thanks to Ross Vander for letting me know
                glewExperimental = GL_TRUE;
                auto status = glewInit();
                if(status != GLEW_OK)
                {
                    log_error("glewInit:" << status << ":" << glewGetErrorString(status));
                    break;
                }

                glOrtho(-200,200,-200,200,-200,200);
                m_IsInit = true;
                return true;
            }
            while(0);
            SDL_GL_DeleteContext(m_MainContext);
            m_MainContext = nullptr;
        }
        while(0);
        SDL_DestroyWindow(m_MainWindow);
        m_MainWindow = nullptr;

    }
    while(0);
    SDL_Quit();
    return false;
}

void tGraphicEnviroment::cleanup()
{
    log_fun_info("");
    // Delete our OpengL context
    SDL_GL_DeleteContext(m_MainContext);

    // Destroy our window
    SDL_DestroyWindow(m_MainWindow);

    // Shutdown SDL 2
    SDL_Quit();
}
