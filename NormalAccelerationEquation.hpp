/// Copyright © 2017 Alexander Kalyuzhnyy. All rights reserved.

#ifndef OUREQUATION_HPP
#define OUREQUATION_HPP
#include <ostream>
#include "MotionEquation.hpp"
#include "BasicHandicap.hpp"
#include "BasicEquation.hpp"
#include "common.hpp"

struct tNormalAccelerationEquation: public tBasicEquation
{
    using tBasicEquation::tBasicEquation;
    virtual int function(double t, const double* input, double* output) override;
};

#endif // OUREQUATION_HPP
